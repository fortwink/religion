<?
use yii\helpers\Html;


$this->title = $model->name;

?>

<?if ($model->type_id == 2) {?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Места реализации религиозной литературы и атрибутики')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
    </div>
    <br><br><br>

    <div class="zdan_block">
        <div class="zdan_photo">
            <? if ($model->getImage() instanceof \rico\yii2images\models\PlaceHolder) {?>
                <div class="zdan_zdan_photo" style="background-image: url('/images/icon/statobject.png');"></div>
            <?} else {?>
                <div class="zdan_zdan_photo" style="background-image: url('<?=$model->getImage()->getPath('300x200')?>');"></div>

            <?}?>
        </div>
        <div class="zdan_text_block">
            <div class="zdan_green_font1">
                <?=$model->name?>
            </div>
            <br><br>

            <br><br>

            <div class="zdan_black_font1">
                <img src="/images/location.png"> <?=$model->address?>
                <div class="zdan_blue_font1"><a href="0"><i>посмотреть на карте</i></a></div>

            </div>
            <br><br>

            <div class="zdan_black_font1">
                <img src="/images/phone-call.png"> <?=$model->contacts?>
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <i><?=$model->confession->tname?></i>
            </div>
            <br><br>


        </div>
    </div>


    <br><br><br>

    <?if ($model->thidden_text && Yii::$app->user->id){?>
        <div style="background-color: #e8e8e8;">
            <div class="zdan_grey_block">
                <?=$model->thidden_text?>
            </div>
        </div>
    <?}?>


    <div class="zdan_white_block">
        <div class="zdan_black_font1">
            <i><b>Описание стационарного объекта</b></i>
        </div>
        <br><br>

        <div class="zdan_black_font2">
            <?=$model->preview_text?><br>
            <?=$model->description?>
        </div>
    </div>


    <style type="text/css">


        .slider {
            width: 90%;
            margin: 0px auto;
            text-align: center;
        }

        .slick-slide {
            width: 100%;
            padding: 5px;
        }

        .slick-slide img {
            width: 100%;

        }

        .slick-prev:before,
        .slick-next:before {
            color: green;

        }



        @media (max-width: 1000px) {
            .slider {
                width: 100%;
            }
        }

    </style>

    <?if (count($model->getImages())>=2){?>

        <div class="zdan_white_block" style="margin-top: -30px;">
            <section class="center slider">
                <?foreach($model->getImages() as $image){?>
                    <div>
                        <?=Html::img('/'.$image->getPath())?>
                    </div>
                <?}?>
            </section>
        </div>
    <?}?>



</div>

<? } else {?>


<div class="main" ng-controller="objectdetail">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Культовые сооружения')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
    </div>
    <br><br><br>

    <div class="zdan_block">
        <div class="zdan_photo">
            <div class="zdan_zdan_photo" style="background-image: url('<?
                $img1 = '';
                if ($model->getImage() instanceof \rico\yii2images\models\PlaceHolder) {


                    switch ($model->confess_id) {
                        case 1:
                            $img1 = '/images/icon/islam.png';
                            break;
                        case 2:
                            $img1 = '/images/icon/hrist.png';
                            break;
                        case 3:
                            $img1 = '/images/icon/bahai.png';
                            break;
                        case 4:
                            $img1 = '/images/icon/krishnaizm.png';
                            break;
                        case 5:
                            $img1 = '/images/icon/buddizm.png';
                            break;
                        case 6:
                            $img1 = '/images/icon/iudaizm.png';
                            break;
                    }
                } else {
                    $img1 = '/'.$model->getImage()->getPath('300x200');
                }
            echo $img1;
            ?>');"></div>

        </div>
        <div class="zdan_text_block">
            <div class="zdan_green_font1">

                <?=$model->tname?>
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <i><b><?=$model->confession->tname?></b></i>
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <img src="/images/location.png"><?=$model->address?>
                <?if ($model->map){?>
                    <div class="zdan_blue_font1"><a href="" ng-click="seeonmap=!seeonmap;"><i ng-show="!seeonmap">посмотреть на карте</i><i ng-show="seeonmap">скрыть карту</i></a></div>
                <?}?>
            </div>
            <div ng-show="seeonmap">
                <script>
                    var coords = [];
                </script>

                <?
                $str = '<script>';
                if ($model->map) {
                    $balloncontent = addcslashes('<img style="float:right;" src="/' . $model->getImage()->getPath('100x100') . '"><b>' . str_replace('"', '', $model->short_name) . '</b><br><i>' . $model->confession->name . '</i><br>' . $model->address . '<br>' .
                        '<a href="/confession/objectview?id=' . $model->id . '">Подробнее</a>', '"');
                    $img = '';
                    switch ($model->confess_id) {
                        case 1:
                            $img = '/images/icon_islam.png';
                            break;
                        case 2:
                            $img = '/images/icon_hrist.png';
                            break;
                        case 3:
                            $img = '/images/icon_bahai.png';
                            break;
                        case 4:
                            $img = '/images/icon_krishna.png';
                            break;
                        case 5:
                            $img = '/images/icon_buddizm.png';
                            break;
                        case 6:
                            $img = '/images/icon_iud.png';
                            break;
                        case 7:
                            $img = '/images/new.png';
                            break;
                    }
                    $str .= '
                            coords.push({
                                point:[' . $model->map . '],
                                ballon: {
                                    balloonContent: "' . $balloncontent . '",
                                    iconContent: "' . str_replace('"', '', $model->short_name) . '"
                                },
                                options: {
                                    iconLayout: "default#image",
                                    iconImageHref: "' . $img . '",
                                    iconImageSize: [60, 60],
                                    iconImageOffset: [-3, -42]
                                }
                                });

                        ';
                }
                $str .= '</script>';

                echo $str;
                ?>
                <script async src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=setupmap"
                        type="text/javascript"></script>
                <script>
                    var map;
                    var myCollection;
                    function setupmap() {

                        //console.log(123);
                        myCollection = new ymaps.GeoObjectCollection();
                        //var myClusterer = new ymaps.Clusterer();

                        var i;
                        var x = 0;
                        var y = 0;
                        var delta = 0;
                        for (i in coords) {
                            //myCollection.add(new ymaps.Placemark(coords[i]));
                            myCollection.add(new ymaps.Placemark(coords[i].point, coords[i].ballon, coords[i].options));
                            if (coords[i].point[0]) {
                                x += (coords[i].point[0]) * 1;
                                y += (coords[i].point[1]) * 1;
                                delta++;
                            }
                        }

                        console.log(delta);
                        if (delta) {
                            x = x / delta;
                            y = y / delta;
                            map = new ymaps.Map("map-wrapper", {
                                center: [x, y],
                                zoom: 10
                            });
                            /*var circle = new ymaps.Circle([[x, y], 50000], {}, {
                             draggable: true
                             });
                             map.geoObjects.add(circle);*/
                            map.geoObjects.add(myCollection);
                        }
                    }
                </script>
                <div id="map-wrapper" style="height: 30em;">

                </div>

            </div>
            <br><br>

            <div class="zdan_black_font1">
                <img src="/images/phone-call.png"> <?=$model->contacts?>
            </div>
            <br><br>

            <div class="zdan_black_font2">
                <?=$model->preview?><br>
            </div>
        </div>
    </div>


    <br><br><br>

    <?if ($model->thidden_text && Yii::$app->user->id){?>
        <div style="background-color: #e8e8e8;">
            <div class="zdan_grey_block">
                <?=$model->thidden_text?>
            </div>
        </div>
    <?}?>


    <?if ($model->desc){?>
        <div class="zdan_white_block">
            <div class="zdan_black_font1">
                <i><b>Описание культового объекта</b></i>
            </div>
            <br><br>

            <div class="zdan_black_font2">
                <?=$model->desc?>

            </div>
        </div>
    <?}?>

    <style type="text/css">


        .slider {
            width: 90%;
            margin: 0px auto;
            text-align: center;
        }

        .slick-slide {
            width: 100%;
            padding: 5px;
        }

        .slick-slide img {
            width: 100%;

        }

        .slick-prev:before,
        .slick-next:before {
            color: green;

        }



        @media (max-width: 1000px) {
            .slider {
                width: 100%;
            }

        }
    </style>


    <?if (count($model->getImages())>=2){?>
        <div class="zdan_white_block" style="margin-top: -30px;">
            <section class="center slider">
                <?foreach($model->getImages() as $image){?>
                    <div>
                        <?=Html::img('/'.$image->getPath())?>
                    </div>
                <?}?>
            </section>
        </div>
    <?}?>


    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function () {

            $(".center").slick({
                dots: false,
                infinite: true,
                centerMode: true,
                slidesToShow: 2,
                slidesToScroll: 2
            });

        });
    </script>


    <br><br><br>
    <? if (count($model->people)) { ?>
        <div class="grey_inf_block_outer">
            <div class="green_h1 relig_desc_title">
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
                <?=Yii::t('app','Руководители')?>
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            </div>

            <div class="relig_missianers_outer"></div>

            <div class="relig_eg_block">
                <br><br>

                <div class="row">

                    <? foreach ($model->people as $religman) { ?>
                        <div class="col-xs-6 col-sm-2">
                            <a href="/confession/missionerview?id=<?=$religman->id?>">
                                <div class="relig_missianers_eg" style="background-image: url('/<?=$religman->getImage()->getPath('130x130')?>');"></div>
                                <div class="relig_text"><?=$religman->name;?></div>
                            </a>
                        </div>
                    <? } ?>

                </div>
                <br>
            </div>
        </div>
    <? } ?>
</div>
<? } ?>