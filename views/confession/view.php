<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
$this->title = $model->name;

?>

<div class="main relig_top_fix" ng-controller="confessview">

    <br><br>

    <div class="relig_header" style="background-image: url('/<?=$model->getImage()->getPath('x185')?>');"></div>
    <br><br><br>
    <?php
        $img = '';
        $color = '';
        switch ($id) {
            case 1:
                $img = '/images/icon_islam.png';
                $color = '#52ac62';
                break;
            case 2:
                $img = '/images/icon_hrist.png';
                $color = '#69c1d3';
                break;
            case 3:
                $img = '/images/icon_bahai.png';
                $color = '#8582bc';
                break;
            case 4:
                $img = '/images/icon_krishna.png';
                $color = '#e8a765';
                break;
            case 5:
                $img = '/images/icon_buddizm.png';
                $color = '#e6d80a';
                break;
            case 6:
                $img = '/images/icon_iud.png';
                $color = '#3b3b3b';
                break;

            case 7:
                $img = '/images/new.png';
                $color = '#f997c2';
                break;
        }

     ?>
    <img src="<?=$img?>" class="relig_icon">

    <div class="relig_desc">

        <div class="green_h1 relig_desc_title" style="color: <?=$color?>"><b><?= $model->tname ?></b></div>
        <br>

        <div class="relig_sub_title"><?= $model->short ?></div>
        <br>




        <div class="relig_hide_desc" ng-show="detail1">
            <?= $model->full ?>
        </div>


        <br>

        <?if ($model->full){?><div class="relig_grey_but" ng-click="detail1=!detail1" id="show_more"><span ng-show="!detail1"><i><?=Yii::t('app','Более детальное описание')?> </i>&or;</span><span ng-show="detail1"><i><?=Yii::t('app','Скрыть')?> </i>&and;</span></div><?}?>
    </div>

    <br><br><br><br>

    <div class="relig_grey_inf_block_outer">

        <div class="blue_h1">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            <?=Yii::t('app','Культовые сооружения');?>
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            <br>
        </div>


        <div class="relig_eg_block">
            <br><br>

            <div class="row">

                <? foreach ($objects4 as $item) { ?>
                    <div class="col-xs-6 col-sm-3">
                        <a href="<?=Url::to(['/confession/objectview','id'=>$item->id]) ?>">
                            <div class="relig_eg"
                             style="background-image: url('/<?= $item->getImage()->getPath('150x150') ?>');"></div>
                        </a>
                        <div class="relig_text"><a
                                href="<?=Url::to(['/confession/objectview','id'=>$item->id]) ?>"><?= $item->tname ?></a></div>
                    </div>
                <? } ?>


            </div>
        </div>
    </div>

    <div class="blue_but2" style="position: relative; top:-25px; font-size: 20px;"><a
            href="<?=Url::to(['/confession/objects','confess'=>$id,'conf'=>$conf]);?>"><?=Yii::t('app','Перейти в раздел');?></a></div>


    <div class="white_inf_block_outer">
        <div class="green_h1 relig_desc_title">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            <?=Yii::t('app','Руководители')?>
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>

        <div class="relig_missianers_outer"></div>

        <div class="relig_eg_block">
            <br><br>

            <div class="row">

                <? foreach ($people6 as $item) { ?>
                    <div class="col-xs-6 col-sm-2">
                        <a href="<?=Url::to(['/confession/missionerview','id'=> $item->id]) ?>">
                            <div class="relig_missianers_eg"
                                 style="background-image: url('/<?= $item->getImage()->getPath('130x130') ?>');"></div>
                            <div class="relig_text"><?= $item->name ?></div>
                        </a>
                    </div>
                <? } ?>

            </div>

            <br>

        </div>


        <div class="green_but2" style="position: relative; top:0px; font-size: 20px;">
            <a href="<?=Url::to(['/confession/missioners','confid'=>$id,'conf'=>$conf])?>"><?=Yii::t('app','Перейти в раздел');?></a>
        </div>

        <br><br><br>

        <div class="green_inf_block_outer">
            <div class="white_h1">
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
                <?=Yii::t('app','Миссионеры')?>
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            </div>
            <br><br>
            <div class="blue_but2" style="position: relative; top:0px; font-size: 20px;">
                <a href="<?=Url::to(['/pages/missioners'])?>"><?=Yii::t('app','Посмотреть список');?></a>
            </div>
        </div>
        <?/*<div class="green_inf_block_outer">
            <div class="white_h1">
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
                Стационарные помещения
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            </div>
            <br><br>

            <div class="relig_green_block_inner">

                <div class="row">
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="/images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="/images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="/images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="/images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                </div>

            </div>
        </div>*/?>


    </div>


</div>