<?
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $model->name;
?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Руководители')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
    </div>
    <br>

    <div class="missioner_block">
        <div class="missioner_photo">

            <div class="relig_missianers_eg_big" style="background-image: url('<?='/'.$model->getImage()->getPath()?>');">
                <?//=Html::img('/'.$model->getImage()->getPath())?>
            </div>

        </div>
        <div class="missioner_text_block">
            <br><br>

            <div class="missioner_green_font1">
                <?=$model->name?>
            </div>
            <br><br>

            <?/*<div class="missioner_black_font2">
                Дата регистрации:
                <div class="missioner_green_font2">
                    22.04.1965
                </div>
            </div>*/?>
            <br><br>

            <div class="missioner_black_font2">
                Гражданство: <?=$model->gr?><br>
                <!--Место регистрации: Казахстан-->
            </div>
            <br><br>
            <div class="missioner_black_font2">
                Должность: <?=$model->position?><br>
                <!--Место регистрации: Казахстан-->
            </div>
            <br><br>
            <div class="missioner_black_font2">
                Дата рождения: <?=$model->bdate?><br>
                <!--Место регистрации: Казахстан-->
            </div>
            <br><br>
            <div class="missioner_black_font2">
                Стаж работы: <?=$model->exp?><br>
                <!--Место регистрации: Казахстан-->
            </div>
            <br><br>

            <div class="missioner_black_font1">
                <i><b><?=$model->confession->tname?></b></i>
            </div>
            <br><br>

            <div class="missioner_black_font1">
                <img src="/images/phone-call.png"> <?=$model->contact_phone?>
            </div>
            <br><br>

            <?if ($model->register && Yii::$app->user->id){?>
            <div class="missioner_black_font1">
                <?=$model->register?>
            </div>
            <br><br>
            <?}?>
            <div class="missioner_black_font1">
                <?=$model->description?>
            </div>
            <br><br>
            <?if ($model->object) {?>
                <div class="missioner_green_font2">
                    <a style="color:#52ac62;" href="<?=Url::to(['/confession/objectview','id'=>$model->object->id])?>;?>"><?=$model->object->tname;?></a>
                </div>
            <?}?>
            <br><br>

            <?/*<div class="missioner_black_font2">
                Дата регистрации:
                <div class="missioner_green_font2">
                    2.04.2000
                </div>
                <br>
                Дата перерегистрации:
                <div class="missioner_green_font2">
                    22.04.2015
                </div>
            </div>*/?>
        </div>
    </div>


    <br><br><br>

    <?if ($model->hidden_info && Yii::$app->user->id){?>
        <div style="background-color: #e8e8e8;">
            <div class="missioner_grey_block">
                <?=$model->hidden_info?>
            </div>
        </div>
    <?}?>
    <?/*<div style="background-color: #e8e8e8;">
        <div class="missioner_grey_block">
            <?//=$model->hidden_info?>
        </div>
    </div>
    */?>

</div>