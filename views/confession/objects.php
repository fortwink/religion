<?
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;

//$this->title = Yii::t('app','Религиозная карта');
if ($confm)
    $this->title = $confm->tname;
else
    $this->title = Yii::t('app','Религиозная карта');

?>
<div ng-controller="objectspage">
    <div class="main">
        <br>
        <br>

        <div class="red_h1">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
                <? if ($confm)
                        echo $confm->tname;
                    else
                        echo Yii::t('app','Религиозная карта')?>
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>
    </div>

    <div class="white_inf_block_outer">


        <div class="relig_map_bar">
            <div class="relig_map_bar_item">
                <ul class="dropdown">
                    <li class="dropdown-top">
                        <a class="dropdown-top" href="#">конфессия &or;</a>
                        <ul class="dropdown-inside">
                            <?
                            $confess = \app\models\Confession::find()->all();
                            foreach ($confess as $con) {
                                ?>
                                <li><a href="<?=Url::to(['/confession/objects','confess'=> $con->id]) ?>"><?= $con->tname ?></a></li>
                                <?
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="relig_map_bar_item">
                <ul class="dropdown">
                    <li class="dropdown-top">
                        <a class="dropdown-top" href="#"><?=Yii::t('app','тип')?> &or;</a>
                        <ul class="dropdown-inside">
                            <?
                            if (Yii::$app->user->isGuest)
                            {
                                $types = \app\models\Types::find()->where(['active'=>1])->all();
                            }
                            else {
                                $types = \app\models\Types::find()->all();
                            }
                            foreach ($types as $t) {
                                ?>
                                <li><a href="<?=Url::to(['/confession/objects','type'=> $t->id]) ?>"><?= $t->tname ?></a></li>
                                <?
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="relig_map_bar_item">
                <ul class="dropdown">
                    <li class="dropdown-top">
                        <a class="dropdown-top" href="#"><?=Yii::t('app','район')?> &or;</a>
                        <ul class="dropdown-inside">
                            <?
                            $regions = \app\models\Region::find()->all();
                            foreach ($regions as $r) {
                                ?>
                                <li><a href="<?=Url::to(['/confession/objects','region'=> $r->id]) ?>"><?= $r->tname ?></a></li>
                                <?
                            }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div>


            <div class="relig_map_bar_form mobile_hide">
                <form action="<?=Url::to(['/confession/objects']) ?>" method="GET" style="display: inline-block;">
                    <input value="<?=(isset($_GET['q']))?$_GET['q']:'';?>" type="text" name="q" size="16" style="border:none; height: 27px">
                    <button type="submit" class="btn btn-secondary"
                            style="height: 27px; padding: 0; background-color: #e8e8e8;"><span
                            class="glyphicon glyphicon-search"></span></button>
                </form>
                <div class="relig_map_bar_form_white_but" ng-click="showmap=!showmap"><span ng-if="!showmap"><?=Yii::t('app','весь список')?></span><span ng-if="showmap"><?=Yii::t('app','на карте')?></span></div>
            </div>
        </div>

        <br><br>
    </div>
    <div ng-show="showmap">
        <div class="container">

            <div class="relig_map2_body">

                <? foreach ($model as $object) { ?>
                    <div class="relig_map2_item">
                        <a href="<?=Url::to(['/confession/objectview','id'=> $object->id])?>">
                            <?if ($object->type_id == 2) {?>
                                <? if ($object->getImage() instanceof \rico\yii2images\models\PlaceHolder) {?>
                                    <div class="pull-left img" style="background-image: url('/images/icon/statobject.png'); background-size: cover; width: 100px; height: 100px;"></div>

                                <?} else {?>
                                    <div
                                        class="pull-left img"><?= Html::img('/' . $object->getImage()->getPath('100x100')) ?></div>
                                <?}?>
                            <?} else {
                                ?>
                                <div
                                    class="pull-left img"><?= Html::img('/' . $object->getImage()->getPath('100x100')) ?></div>
                                <?
                            }?>

                        </a>

                        <div class="desc">
                            <a href="<?=Url::to(['/confession/objectview','id'=> $object->id]) ?>"><?= $object->tshortname ?></a>
                            <br>
                            <i><?= $object->confession->tname ?></i>
                            <br>
                            <?/*<a href="" style="
                        display: inline-block;
                        font-size: 12px;
                        float: left;
                        color:#69c1d3;
                        text-decoration: underline;"><?=Yii::t('app','Смотреть на карте')?></a>*/?>
                        </div>
                    </div>
                <? } ?>


            </div>


            <br><br>
            <? echo LinkPager::widget([
                'pagination' => $pages,
                'options' => [
                    'class' => 'pages',
                    //''
                ]
            ]); ?>
        </div>

    </div>

    <div ng-show="!showmap">


        <script>
            var coords = [];
        </script>
        <?
        //echo '<pre>'.print_r($modelraw,1).'</pre>';
        $str = '<script>';
        foreach ($modelraw as $m) {

            if ($m->map) {
                $balloncontent = addcslashes('<img style="float:right;" src="/' . $m->getImage()->getPath('100x100') . '"><b>' . str_replace('"', '', $m->tshortname) . '</b><br><i>' . $m->confession->tname . '</i><br>' . $m->address . '<br>' .
                    '<a href="/confession/objectview?id=' . $m->id . '">'.Yii::t('app','Подробнее').'</a>', '"');
                $img = '';
                switch ($m->confess_id) {
                    case 1:
                        $img = '/images/icon_islam.png';
                        break;
                    case 2:
                        $img = '/images/icon_hrist.png';
                        break;
                    case 3:
                        $img = '/images/icon_bahai.png';
                        break;
                    case 4:
                        $img = '/images/icon_krishna.png';
                        break;
                    case 5:
                        $img = '/images/icon_buddizm.png';
                        break;
                    case 6:
                        $img = '/images/icon_iud.png';
                        break;
                    case 7:
                        $img = '/images/new.png';
                        break;
                }
                /* '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                 '<i>'+item.confession+'</i><br>'+
                 item.address+'<br>'+
                 '<a href="/confession/objectview?id='+item.id+'">Подробнее>></a>'*/
                $str .= '
                            coords.push({
                                point:[' . $m->map . '],
                                ballon: {
                                    balloonContent: "' . $balloncontent . '",
                                    iconContent: "' . str_replace('"', '', $model->tshortname) . '"
                                },
                                options: {
                                    iconLayout: "default#image",
                                    iconImageHref: "' . $img . '",
                                    iconImageSize: [60, 60],
                                    iconImageOffset: [-3, -42]
                                }
                                });

                        ';
            }
        }
        echo $str . '</script>';
        ?>
        <script async src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=setupmap"
                type="text/javascript"></script>
        <script>
            var map;
            var myCollection;
            function setupmap() {

                myCollection = new ymaps.GeoObjectCollection();
                //var myClusterer = new ymaps.Clusterer();

                var i;
                var x = 0;
                var y = 0;
                var delta = 0;
                for (i in coords) {
                    //myCollection.add(new ymaps.Placemark(coords[i]));
                    myCollection.add(new ymaps.Placemark(coords[i].point, coords[i].ballon, coords[i].options));
                    if (coords[i].point[0]) {
                        x += (coords[i].point[0]) * 1;
                        y += (coords[i].point[1]) * 1;
                        delta++;
                    }
                }

                if (delta) {
                    x = x / delta;
                    y = y / delta;
                    map = new ymaps.Map("map-wrapper", {
                        center: [x, y],
                        zoom: 10
                    });
                    /*var circle = new ymaps.Circle([[x, y], 50000], {}, {
                     draggable: true
                     });
                     map.geoObjects.add(circle);*/
                    map.geoObjects.add(myCollection);
                }
            }
        </script>
        <div id="map-wrapper" style="height: 30em;">

        </div>


    </div>
    <br><br><br>
</div>