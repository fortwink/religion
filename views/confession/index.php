<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = Yii::t('app','Религия и общество');
?>

<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Религия и общество')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2"><?=Yii::t('app','Здесь собраны все официально зарегестрированные конфессии города Алматы')?>
        </div>
    </div>
    <br><br>

    <div class="religions_grey_block_top_icon_outer">
        <div class="religions_icon_inline">
                <div class="religions_icon_block">
                    <img src="/images/icon_hrist.png">


                    <div class="religions_icon_text" ><a style="color:#69c1d3" href="<?=Url::to(['/confession/view','id'=>2])?>"><?=Yii::t('app','ХРИСТИАНСТВО')?></a></div>


                </div>
        </div>
    </div>


    <div class="religions_grey_block_outer">
        <div class="religions_grey_block_inner">


            <div class="religions_grey_block_inner_links">
                Православие<br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>1])?>"><?=Yii::t('app','Православная церковь')?></a><br>

            </div>


            <div class="religions_grey_block_inner_links">
                Католицизм<br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>2])?>"><?=Yii::t('app','Римско-католическая церковь')?></a><br>
            </div>


            <div class="religions_grey_block_inner_links">
                Протестанизм<br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>7])?>"><?=Yii::t('app','Адвентисты Седьмого Дня')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>10])?>"><?=Yii::t('app','Евангельско-Лютеранская церковь')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>11])?>"><?=Yii::t('app','Пресвитериане')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>12])?>"><?=Yii::t('app','Пятидесятники')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>13])?>"><?=Yii::t('app','Евангельские Христиане-Баптисты')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>14])?>"><?=Yii::t('app','Методисты')?></a><br>
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>3])?>"><?=Yii::t('app','Новоапостольская церковь')?></a><br>


            </div>


            <div class="religions_grey_block_inner_links">

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>4])?>"><?=Yii::t('app','Старообрядческая церковь')?></a><br>

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>2,'conf'=>5])?>"><?=Yii::t('app','Армянская церковь')?></a><br>

            </div>


            <br><br><br>
        </div>
    </div>


    <br>

    <div class="religions_grey_block_bottom_icon_outer">

        <div class="religions_icon_inline">
                <div class="religions_icon_block">
                    <img src="/images/icon_islam.png">


                    <div class="religions_icon_text" ><a style="color:#52ac62; " href="<?=Url::to(['/confession/view','id'=>1])?>">ИСЛАМ</a></div>


                </div>
        </div>


        <div class="religions_icon_inline">
                <div class="religions_icon_block">
                    <img src="/images/icon_buddizm.png">


                    <div class="religions_icon_text" ><a style="color:#e6d80a; " href="<?=Url::to(['/confession/view','id'=>5])?>">БУДДИЗМ</a></div>


                </div>

        </div>

        <?/*<div class="religions_icon_inline">
                <div class="religions_icon_block">
                    <img src="/images/icon_bahai.png">


                    <div class="religions_icon_text" ><a style="color:#8582bc; " href="<?=Url::to(['/confession/view','id'=>3])?>">БАХАИ</a></div>


                </div>
        </div>*/?>

        <div class="religions_icon_inline">
                <div class="religions_icon_block">
                    <img src="/images/icon_iud.png">


                    <div class="religions_icon_text" ><a style="color:#3b3b3b; " href="<?=Url::to(['/confession/view','id'=>6])?>"><?=Yii::t('app','ИУДЕЙСКАЯ <br> РЕЛИГИЯ')?></a></div>


                </div>
        </div>

       <?/* <div class="religions_icon_inline religions_icon_block_mob_fix">
                <div class="religions_icon_block">
                    <img src="/images/icon_krishna.png">


                    <div class="religions_icon_text" ><a style="color:#e8a765; " href="<?=Url::to(['/confession/view','id'=>4])?>"><?=Yii::t('app','ОБЩЕСТВО <br> СОЗНАНИЯ КРИШНЫ')?></a></div>


                </div>
        </div>*/?>

    </div>

    <div class="religions_grey_block_top_icon_outer">
        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="/images/new.png">


                <div class="religions_icon_text" ><a style="color:#f997c2" href="<?=Url::to(['/confession/view','id'=>7])?>"><?=Yii::t('app','НОВЫЕ РЕЛИГИИ')?></a></div>


            </div>
        </div>
    </div>


    <div class="religions_grey_block_outer">
        <div class="religions_grey_block_inner">

            <div class="religions_grey_block_inner_links">

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>7,'conf'=>6])?>"><?=Yii::t('app','Свидетели Иеговы')?></a><br>

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>7,'conf'=>8])?>"><?=Yii::t('app','Мормоны')?></a><br>

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>7,'conf'=>15])?>"><?=Yii::t('app','Общество Сознания Кришны')?></a><br>

                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>7,'conf'=>16])?>"><?=Yii::t('app','Бахаи')?></a><br>




                <img src="/images/blue_rumb.png" width="15px">
                <a href="<?=Url::to(['/confession/view','id'=>7,'conf'=>9])?>"><?=Yii::t('app','Муниты')?></a><br>



            </div>

            <br><br><br>
        </div>
    </div>
</div>
<div class="green_inf_block_outer">
    <div class="white_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Места реализации религиозной литературы и атрибутики')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>
    <div class="blue_but2" style="position: relative; top:0px; font-size: 20px;">
        <a href="<?=Url::to(['/confession/objects','type'=>2])?>"><?=Yii::t('app','Посмотреть список');?></a>
    </div>
</div>
