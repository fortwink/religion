<?
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $model->name;
?>
<div class="main">
    <br>
    <br>


    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Новости и видео')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>

    </div>
    <br><br>

    <div class="news_block_page">
        <div class="news_page_title"><?=$model->name;?></div>
        <div class="news_page_menu"><a href="<?=Url::to(['/news'])?>"><< <?=Yii::t('app','ко всем новостям')?></a></div>
        <br><br><br>

        <?
            $img = $model->getImage();
            echo Html::img('/'.$img->getPath('350x270'),['class'=>'news_block_page_img']);
        ?>

        <div class="news_page_date">
            <i><?=implode('.',array_reverse(explode('.',$model->date)))?></i>
            <img src="/images/ic_vk.png" style="float:right; padding-left: 5px;">
            <img src="/images/ic_fb.png" style="float:right; padding-left: 5px;">
            <img src="/images/ic_ok.png" style="float:right; padding-left: 5px;">
            <img src="/images/ic_in.png" style="float:right; padding-left: 5px;">
            <img src="/images/ic_whats.png" style="float:right; padding-left: 5px;">
        </div>

        <div>
            <?=$model->detail?>
        </div>

    </div>


    <div class="news_page_pagination">
        <? if ($model->prev) {?><a href="<?=Url::to(['/news/view','id'=>$model->prev->id])?>">< <?=Yii::t('app','предыдущая')?></a><?}?>
        <img src="/images/arr_none.png">
        <? if ($model->next) {?><a href="<?=Url::to(['/news/view','id'=>$model->next->id])?>"><?=Yii::t('app','следующая')?> ></a><?}?>
    </div>

    <br><br>
</div>