<?
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = Yii::t('app','Новости и видео');
?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Новости и видео')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>

    </div>
    <br><br>

    <div class="news_menu">
        <a href="?filter=1" <?if ($filter == 1) {?> style="color: #e40424; text-decoration: underline;"<?}?>><?=Yii::t('app','Новости')?></a>
        <a href="?filter=2" <?if ($filter == 2) {?> style="color: #e40424; text-decoration: underline;"<?}?>><?=Yii::t('app','Видео')?></a>
        <a href="?filter=0" <?if ($filter != 1 && $filter != 2) {?> style="color: #e40424; text-decoration: underline;"<?}?>><?=Yii::t('app','Весь список')?></a>
    </div>
    <br>

    <div class="news_block">


        <?foreach ($model as $new) {?>
            <div class="news_item">
                <div class="news_item_image_div" style="background-image: url('<?
                    $image = $new->getImage();
                    echo '/'.$image->getPath('230x165');

                ?>');"></div>
                <div class="news_item_title"><a href="<?=Url::to(['/news/view','id'=>$new->id])?>"><?=$new->name?></a></div>
                <div class="news_item_text">
                    <?=$new->preview;?>
                </div>
                <div class="news_item_more"><i><a href="<?=Url::to(['/news/view','id'=>$new->id])?>"><?=Yii::t('app','Подробнее')?>>></a></i></div>
            </div>
        <?}?>





        <br><br>
        <br><br>


        <? echo LinkPager::widget([
            'pagination' => $pages,
            'options' => [
                'class'=>'pages',
                //''
            ]
        ]); ?>

        <br><br><br>


    </div>
</div>