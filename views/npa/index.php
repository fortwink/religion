<?php
/* @var $this yii\web\View */
use yii\widgets\LinkPager;

$this->title = Yii::t('app','Нормативно-правовые акты');
?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Нормативно правовые акты');?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
        <div class="black_h2" style="width: 500px; margin: 0 auto; padding: 10px;"><?=Yii::t('app','Список нормативно правовых документов и другое текстовое пояснение');?></div>


    </div>
</div>
<br>

<div class="docs_body">
    <br>

    <?foreach($model as $i=>$npa){?>

        <div class="docs_item">
            <img src="/images/<? if (strpos($npa->tfile,'.pdf')) { echo 'doc_green.png'; } else echo 'doc_grey.png';?>">
            <a href="/<?=$npa->tfile?>"><?=$npa->tname?></a>
        </div>

        <?/*<div class="docs_item"><img src="images/doc_grey.png"><a href=""><?=$i+1?> <?=$npa->name?></a></div>*/?>


    <?}?>


</div>
<br><br>

<? echo LinkPager::widget([
    'pagination' => $pages,
    'options' => [
        'class'=>'pages',
        //''
    ]
]); ?>
<br><br><br>