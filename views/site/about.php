<?
$this->title = 'О проекте';
?>
<br><br>
<div class="main">
    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        О проекте
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2">Cервис, который позволяет вам найти и узнать больше информации
            о религиозных объектах города Алматы
        </div>
        <br>

    </div>
</div>


<div class="p_eg_block">
    <div class="row">
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg1"></div>
            <div class="p_eg_text">Здесь вы можете найти
                официальный перечень
                конфессий
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg2"></div>
            <div class="p_eg_text">На карте указаны все
                культовые сооружения
                с подробной информацией
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg3"></div>
            <div class="p_eg_text">Информация
                о миссионерах
                и их деятельности
                на территории РК
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg4"></div>
            <div class="p_eg_text">Собрана база данных
                религиозных помещений
                различного назначения
            </div>
        </div>
    </div>
</div>


<div class="grey_inf_block_outer">
    <div class="grey_inf_block_inner">
        <div class="grey_inf_block_h1">О ПРОЕКТЕ</div>
        <br>

        <p>Описание целей проекта интерактивных карт.</p>

        <p>Интерактивная религиозная карта – это уникальный проект, направленный на оптимизация мониторинга религиозной ситуации по городу, а также вовлечение горожан в процесс мониторинга.
</p>

        <p>Сайт создан как структурированная база данных, где периодически будут проходить обновления и корректировки. Здесь вы можете найти:</p>

<ul>
    <li>все 178 зарегистрированных религиозных объединений;<br>
    </li>
    <li>
    <p>
        ареал деятельности миссионеров;
    </p>
    </li>
    <li>
    <p>
        зарегистрированные места (магазины) реализации религиозной литературы и атрибутики;
    </p>
    </li>
    <li>
    <p>
        незарегистрированные места (магазины) реализации религиозной литературы и атрибутики;
    </p>
    </li>
    <li>
    <p>
        места наибольшей концентрации приверженцев нетрадиционных течений;
    </p>
    </li>
    <li>молельные комнаты вне культовых сооружений, в том числе действующие без разрешения.<br>
    </li>
</ul>

<p>Основная фишка - возможность зарегистрированным пользователям оставлять заявку в случае выявления «новых» (незарегистрированных или ранее неизвестных) мест, где совершаются религиозные обряды и ритуалы, действуют миссионеры, реализуется религиозная литература и атрибутика.</p>
    </div>

</div>


<div class="white_inf_block_outer">

    <div class="green_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Ситуация в городе
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="grey_inf_block_inner" style="padding-bottom: 0;">
        <p>В Алматы наблюдается устойчивая тенденция роста религиозности населения. Основная масса верующих горожан на сегодняшний день состоит из мусульман и православных христиан.</p>
        <p>В мегаполисе функционируют 178 зарегистрированных религиозных объединений, которые представлены 17 конфессиями. 
Весьма примечательным является тот факт, что всего по Республике имеется 18 конфессий. Отсюда вытекает, что около 95% всех имеющихся в стране конфессий в той или иной мере осуществляют свою религиозную деятельность на территории нашего города.</p>
    </div>


    <div class="reg_map">
        <div class="aria_map_1" id="r1">Жетысуйский</div>
        <div class="aria_map_2" id="r2">Алатауский</div>
        <div class="aria_map_3" id="r3">Ауэзовский</div>
        <div class="aria_map_4" id="r4">Наурызбайский</div>
        <div class="aria_map_5" id="r5">Бостандыкский</div>
        <div class="aria_map_6" id="r6">Турскибский</div>
        <div class="aria_map_7" id="r7">Алмалинский</div>
        <div class="aria_map_8" id="r8">Медеуский</div>


        <img class="reg_map_img" id="rimage" src="/images/about_map/jetisu.png">


        <div class="reg_map_text">
            <div class="aria_map_1_text">
                <div class="aria_map_text_title">11%</div>
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_2_text">
                <div class="aria_map_text_title">2%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_3_text">
                <div class="aria_map_text_title">3%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_4_text">
                <div class="aria_map_text_title">4%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_5_text">
                <div class="aria_map_text_title">5%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>

        <div class="reg_map_text">
            <div class="aria_map_6_text">
                <div class="aria_map_text_title">6%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_7_text">
                <div class="aria_map_text_title">7%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_8_text">
                <div class="aria_map_text_title">8%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>
    </div>


</div>



<div class="green_inf_block_outer">


    <div class="white_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Поддержка проекта
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="green_inf_block_inner">
        <p color="green"><font color="#007705">«Центр изучения и анализа» Управления по делам религий города
                Алматы.</font></p>

        <p>Предметом деятельности Центра является осуществление научных исследований в сфере религии.
            Целью деятельности Центра является оценка и анализ религиозной ситуации в городе Алматы, а также проведение
            разъяснительных работ.</p>
    </div>
    <br>
    <a href="http://www.ciaudr.kz/about/">www.ciaudr.kz/about/</a>
</div>




<div class="white_inf_block_outer">

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Если возникли вопросы
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br class="mobile_hide"><br class="mobile_hide">
    </div>
    <br><br>


    <div class="quation_block_1">
        По любым вопросам
        вы можете воспользоваться
        данными контактами
    </div>

    <div class="quation_block_2">
        <img src="/images/location.png"> г. Алматы, ул. Толе-би 155, (бывшее здание БТИ) оф.607, 6 этаж<br><br>
        <img src="/images/phone-call.png"> +7 (727) 378-46-14, +7 (727) 378-48-06<br><br>
        <img src="/images/message.png"> centr.ddralmaty@mail.ru<br>
    </div>


</div>