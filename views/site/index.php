<?

use yii\helpers\Url;

$this->title = Yii::t('app','Религиозная карта');

?>
<div class="main" ng-controller="mainpage">


    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Религиозная карта')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2" style="margin: 0 auto; padding: 10px;"><?=Yii::t('app','Cервис, который позволяет вам найти и узнать больше информации о религиозных объектах города Алматы')?>
        </div>
    </div>

    <br><br>

    <div class="index_green_block">

        <div class="row">
            <div class="col-lg-6 col-xs-12 ">

                <div class="index_green_block_item">
                    <a href="<?=Url::to(['/confession'])?>"><div class="index_green_block_item_text"><?=Yii::t('app','официальный перечень конфессий')?>
                    </div></a>
                </div>
                <div class="index_green_block_item">
                    <a href="<?=Url::to(['/confession/objects'])?>"><div class="index_green_block_item_text"><?=Yii::t('app','культовые сооружения с подробной информацией')?>
                    </div>
                    </a>
                </div>
                <div class="index_green_block_item">
                    <a href="<?=Url::to(['/pages/missioners'])?>"><div class="index_green_block_item_text"><?=Yii::t('app','миссионеры и их деятельность на территории Алматы')?>
                    </div></a>
                </div>
                <div class="index_green_block_item">
                    <a href="<?=Url::to(['/confession/objects','type'=>2])?>"><div class="index_green_block_item_text"><?=Yii::t('app','база данных религиозных помещений различного назначения')?>
                    </div></a>
                </div>

            </div>


            <div class="col-lg-6 col-xs-12">

                <img src="/images/green_background_img.png" class="mobile_hide" style="margin:0 auto;">
            </div>


        </div>
    </div>


    <br><br>

    <div class="white_inf_block_outer">

        <div class="index_black_h1">
            <div class="h1_rombs"><font color="#69c1d3">&#9830; &#9830; &#9830; &#9830;</font></div>
<?=Yii::t('app','Вы можете начать пользоваться')?>
            <div class="h1_rombs"><font color="#69c1d3">&#9830; &#9830; &#9830; &#9830;</font></div>
            <br>
            <font color="#69c1d3"><?=Yii::t('app','информационным сервисом')?></font> <?=Yii::t('app','прямо сейчас')?>
        </div>

        <br>
    </div>


    <div class="relig_map_bar" id="relig_map_bar">
        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#">конфессия &or;</a>
                    <ul class="dropdown-inside">
                        <li ng-repeat="c in filters.confess"><span ng-click="setConfess(c.id)">{{c.name}}</span></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#"><?=Yii::t('app','тип')?> &or;</a>
                    <ul class="dropdown-inside">
                        <li ng-repeat="t in filters.type"><span ng-click="setType(t.id)">{{t.name}}</span></li>

                    </ul>
                </li>
            </ul>
        </div>

        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#"><?=Yii::t('app','район')?> &or;</a>
                    <ul class="dropdown-inside">
                        <li ng-repeat="r in filters.region"><span ng-click="setRegion(r.id)">{{r.name}}</span></li>

                    </ul>
                </li>
            </ul>
        </div>


        <div class="relig_map_bar_form mobile_hide">
        <input ng-model="q" size="16" style="border:none; height: 27px">
        <button class="btn btn-secondary" ng-click="setQ()"
                style="height: 27px; padding: 0; background-color: #e8e8e8;  "><span
                class="glyphicon glyphicon-search"></span></button>
            <div class="relig_map_bar_form_white_but"><a href="<?=Url::to(['/confession/objects'])?>">весь список</a></div>
        </div>
    </div>

    <div style="width: 100%; height: 400px; border:1px solid black; text-align: center;">



        <yandex-map id="mainpage-map" center="map.center" options="{scrollwheel: false}" zoom="map.zoom">
            <ymap-marker ng-repeat="marker in markers" index="$index+1" coordinates="marker.coordinates" properties="marker.properties" options="marker.options"></ymap-marker>

        </yandex-map>
       <?/* <div id="map" style="width: 100%; height: 400px"></div>*/?>


    </div>

    <div class="white_inf_block_outer">

        <a href="<?=Url::to(['/confession/index'])?>"><div class="blue_but2" style="position: relative; top:-65px; font-size: 20px;"><?=Yii::t('app','Перейти в раздел')?></div></a>

        <br>

        <div class="index_inf_block center_align_mobile"><img src="/images/index_comp.png"></div>
        <div class="index_inf_block center_align_mobile">
            <div class="index_black_h1">
<?=Yii::t('app','Достоверная информация')?>
                <br>
                <font color="#007705" style="font-size: 15px;letter-spacing: 5px;">&#9830; &#9830; &#9830; &#9830;</font>
            </div>
            <p><?=Yii::t('app','Нам важно, чтобы вы получали только актуальные данные, поэтому в случае, если вы увидели некорректные данные, или отсутствующий в базе объект - просьба обратиться к нам, и наши специалисты свяжутся c вами и обязательно проверят достоверность данных или идентифицируют новый объект.')?></p>

            <a href="<?=Url::to(['/site/request'])?>"><div class="index_green_but"><?=Yii::t('app','оставить заявку')?></div></a>
        </div>


    </div>


    <div class="green_inf_block_outer">


        <div class="white_h1">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            <?=Yii::t('app','Религия и общество')?>
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>
        <br><br>

        <div class="green_inf_block_inner">
            <p color="green"><font color="#007705"><?=Yii::t('app','Многообразие - не есть недостаток, наоборот, это самый неоценимый дар Божий, благодаря которому происходит настоящее взаимообогащение, подлинное развитие. Каждый народ обладает неповторимыми, присущими только ему традициями, историей, складом мышления. Каждый вносит свою особую мелодию в полифинию культуры человечества.” (с)')?></font></p>

            <p><?=Yii::t('app','Выступление Президента РК Н.А.Назарбаева на закрытии I Съезда лидров мировых и традиционных религий')?></p>
        </div>
        <br>

        <div class="index_green_reg_icon">
            <img src="/images/islam.png">
            <br>
            <br>
            <a href="<?=Url::to(['/confession/view','id'=>1])?>">Ислам</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="/images/bible.png">
            <br>
            <br>
            <a href="<?=Url::to(['/confession/view','id'=>2])?>">Христианство</a>
        </div>



        <div class="index_green_reg_icon">
            <img src="/images/new_icon.png">
            <br>
            <br>
            <a href="<?=Url::to(['/confession/view','id'=>7])?>">Новые религии</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="/images/om.png">
            <br>
            <br>
            <a href="<?=Url::to(['/confession/view','id'=>5])?>">Буддизм</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="/images/star-of-david.png">
            <br>
            <br>
            <a href="<?=Url::to(['/confession/view','id'=>6])?>">Иудаизм</a>
        </div>


    </div>
</div>


<div style="text-align: center;">

    <div class="index_grey_inf_block">
        <div class="index_grey_inf_block_big_font"><b><?=Yii::t('app','Нормативно-правовые акты')?></b></div>
        <br>
        <div class="row">
            <?$cols = array_chunk($npa,4);

            foreach($cols as $npa_col){?>
                <div class="col-md-6 text-left">
                    <?foreach($npa_col as $n){?>
                        <img src="/images/blue_rumb.png">
                        <a href="<?=$n->tfile?>"><?=$n->tname?></a>
                        <br><br>
                    <?}?>
                </div>
            <?}?>
        </div>
        <div class="index_grey_inf_block_small_font"><?=Yii::t('app','Больше документов и информации доступно внутри раздела')?></div>
        <a style="text-align: center;" href="<?=Url::to(['/npa'])?>"><div class="blue_but2" style="position: relative; top:45px; margin: 0 auto;"><?=Yii::t('app','Перейти в раздел')?></div></a>
    </div>


</div>

<br><br>

<div class="white_inf_block_outer">

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
<?=Yii::t('app','Видео')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>

    <br><br>

    <div class="video_aria">

        <div class="video-responsive">
            <?=$video->ttext?>
        </div>

    </div>


    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
        }

        .slider {
            width: 90%;
            margin: 20px auto;
        }

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
            color: green;
        }
    </style>

    <br><br><br>

    <div class="green_h1"><?=Yii::t('app','Партнёры')?></div>

    <div style="background-color: #e8e8e8; padding: 20px; margin-top: 40px;">

        <section class="center slider">
            <?foreach ($partners as $p) {?>
            <div>
                <?=\yii\helpers\Html::img('/'.$p->getImage()->getPath('350x150'))?>
            </div>
            <?}?>


        </section>

    </div>
</div>
