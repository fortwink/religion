<?
$this->title = 'Жоба туралы ';
?>
<br><br>
<div class="main">
    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Жоба туралы
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2">
            Алматы қаласындағы діни нысандардың  қызметі туралы қосымша ақпаратты табуға және оқуға мүмкіндік беретін қызмет
        </div>
        <br>

    </div>
</div>


<div class="p_eg_block">
    <div class="row">
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg1"></div>
            <div class="p_eg_text">Мұнда конфессиялардың ресми тізімін таба аласыз
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg2"></div>
            <div class="p_eg_text">Картада ғибадат орындары  туралы  барлық ақпараттар нақты көрсетілген
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg3"></div>
            <div class="p_eg_text">ҚР аумағындағы  миссионерлер  және олардың  қызметi  туралы мәліметтер
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg4"></div>
            <div class="p_eg_text">Түрлі бағыттағы діни ғимараттардың дерекқоры жиналған
            </div>
        </div>
    </div>
</div>


<div class="grey_inf_block_outer">
    <div class="grey_inf_block_inner">
        <div class="grey_inf_block_h1">ЖОБА ТУРАЛЫ</div>
        <br>

        <p>Интерактивті картаның жобасының мақсатын сипаттау.

        <p>Интерактивті діни карта - қаланың діни ахуалына мониторинг жасауға оңтайландыруға, сондай-ақ қалалықтарды  мониторинг процесіне тартуға бағытталған бірегей жоба.

        <p>Сайт құрылымдық деректер қорынан  құрылған, мерзімді жаңартулар мен түзетулер жасалынып тұрады.

        <p>Мұнда сіз таба аласыз:
        <ul>
            <li>Барлық 178 тіркелген діни бірлестіктерді;
            <li>миссионерлердің қызмет саласын;
            <li>діни әдебиеттер мен атрибутиканы сататын тіркелген орындарды (дүкендерді)
            <li>діни әдебиеттер мен атрибутиканы сататын тіркелмеген орындарды (дүкендерді)
            <li>дәстүрлі емес ағымдардын  жақтаушылары тығыз концентрацияланған орындарды;
            <li>рұқсатсыз жұмыс істейтіндерді, соның ішінде ғибадатханадан тыс сыйыну бөлмелерін.
        </ul>
        <p>Басты ерекшелігі - «жаңа» (тіркелмеген немесе бұрын белгісіз)  орындарда діни рәсімдер мен салт-жоралар өткізу, діни әдебиеттер мен төлбелгілерін сату, миссионерлік қызметтерді іске асыру әрекеттерін анықтағанда,  тіркелген пайдаланушылардың өтініш қалдыру мүмкіндігі бар.

    </div>

</div>


<div class="white_inf_block_outer">

    <div class="green_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Қаладағы жағдай
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="grey_inf_block_inner" style="padding-bottom: 0;">
        <p>Алматыда діндар халықтың тұрақты өсу үрдісі байқалады. Бүгінгі күні қалалық сенушілердің негізгі бөлігін мұсылмандар мен православтық христиандар құрайды.</p>
        <p>Мегаполисте 17 конфессияны таныстыратын 178 тіркелген діни бірлестіктер қызмет жасап жатыр.

            Бірақ есте қаларлық факт  - Республика бойынша 18 конфессия бар. Осы жерден шығатын қорытынды бойынша, елімізде бар конфессиялардың 95  % өз діни қызметтерін қандай да бір көлемде біздің қаламыздың территориясында атқарады
        </p>
    </div>


    <div class="reg_map">
        <div class="aria_map_1" id="r1">Жетысуйский</div>
        <div class="aria_map_2" id="r2">Алатауский</div>
        <div class="aria_map_3" id="r3">Ауэзовский</div>
        <div class="aria_map_4" id="r4">Наурызбайский</div>
        <div class="aria_map_5" id="r5">Бостандыкский</div>
        <div class="aria_map_6" id="r6">Турскибский</div>
        <div class="aria_map_7" id="r7">Алмалинский</div>
        <div class="aria_map_8" id="r8">Медеуский</div>


        <img class="reg_map_img" id="rimage" src="/images/about_map/jetisu.png">


        <div class="reg_map_text">
            <div class="aria_map_1_text">
                <div class="aria_map_text_title">11%</div>
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_2_text">
                <div class="aria_map_text_title">2%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_3_text">
                <div class="aria_map_text_title">3%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_4_text">
                <div class="aria_map_text_title">4%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_5_text">
                <div class="aria_map_text_title">5%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>

        <div class="reg_map_text">
            <div class="aria_map_6_text">
                <div class="aria_map_text_title">6%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_7_text">
                <div class="aria_map_text_title">7%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_8_text">
                <div class="aria_map_text_title">8%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>
    </div>


</div>



<div class="green_inf_block_outer">


    <div class="white_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Жобаны қолдау
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="green_inf_block_inner">
        <p color="green"><font color="#007705">Алматы қаласы Дін істері жөніндегі басқармасының «Сараптама және зерттеу» орталығы</font></p>

        <p>
            Орталық қызметінің нысанасы дін саласындағы ғылыми зерттеулерді жүзеге асыру болып табылады. Орталықтың мақсаты Алматы қаласындағы діни ахуалды бағалау және талдау, сондай-ақ ақпараттық-насихат жұмысын жүргізу.
        </p>
    </div>
    <br>
    <a href="http://www.ciaudr.kz/about/">www.ciaudr.kz/about/</a>
</div>




<div class="white_inf_block_outer">

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Егер сіздің сұрақтарыңыз болса
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br class="mobile_hide"><br class="mobile_hide">
    </div>
    <br><br>


    <div class="quation_block_1">
        Барлық сұрақтар бойынша осы байланысты қолдана аласыз.
    </div>

    <div class="quation_block_2">
        <img src="/images/location.png"> Алматы қ. Төле-би к. 155 (бұрынғы ТТБ ғимараты) 607 оф., 6 қабат<br><br>
        <img src="/images/phone-call.png"> +7 (727) 378-46-14, +7 (727) 378-48-06<br><br>
        <img src="/images/message.png"> centr.ddralmaty@mail.ru<br>
    </div>


</div>