<?
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use \msvdev\widgets\mappicker\MapInput;

$this->title = Yii::t('app','Заявка на проверку');

?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <?=Yii::t('app','Заявка на проверку')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
        <div class="black_h2"><?=Yii::t('app','Заполните заявку и дополните карту новыми данными')?></div>
    </div>
    <br><br>
    <div style="color:#484848; font-size: 14px; max-width: 600px; width: auto; margin: 0 auto; padding: 10px;">
        <p><?=Yii::t('app','Нам важно, чтобы вы получали только актуальные данные, поэтому в случае, если вы увидели некорректные данные, или отсутствующий в базе объект - просьба обратиться к нам, и наши специалисты свяжутся в вами и обязательно проверят достоверность данных или идентифицируют новый объект.')?>
        </p>
    </div>



</div>



<div class="white_inf_block_outer">
    <?
    ?>
    <?php $form = ActiveForm::begin([
        'id' => 'contact-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{input}",
            'options'=>['class'=>''],
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
        <!--<input type="text" name="" class="grey_form_input" placeholder="ФИО">-->
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['class' => 'grey_form_input ','placeholder'=>Yii::t('app','ФИО')]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['class' => 'grey_form_input ','placeholder'=>'E-mail']) ?>
            </div>

        </div>
       <!-- <input type="text" name="" class="grey_form_input" placeholder="Адрес местоположения">-->
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'phone')->textInput(['class' => 'grey_form_input ','placeholder'=>'7 (_ _ _ ) _ _ _  _ _  _ _']) ?>
            </div>

        </div>

    <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'body')->textarea(['class' => 'grey_form_textarea ','placeholder'=>Yii::t('app','Сообщение')]) ?>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput() ?>
        </div>
    </div>
        <!--<input type="text" name="" class="grey_form_input" placeholder="7 (_ _ _ ) _ _ _  _ _  _ _">-->
        <br>

        <!--<table style="margin:0 auto;">
            <tr>
                <td>
                    <textarea class="grey_form_textarea"  placeholder="Сообщение"></textarea>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <input type="file" name="" style="width: 97%" class="grey_form_input"/>
                </td>
            </tr>
        </table>-->
    <?
    echo $form->field($model, 'map')->widget(
        MapInput::className(),
        [
            'language' => 'ru-RU', // map language, default is the same as in the app
            'service' => 'yandex', // map service provider, "google" or "yandex", default "google"
            //'apiKey' => '', // required google maps
            //'coordinatesDelimiter' => ',', // attribute coordinate string delimiter, default "@" (lat@lng)
            'mapWidth' => '100%', // width map container, default "500px"
            'mapHeight' => '300px', // height map container, default "500px"
            'mapZoom' => '10', // map zoom value, default "10"
            'mapCenter' => [43.308129342096706,76.881422996521], // coordinates center map with an empty attribute, default Moscow
        ]
    );
    ?>

        <br>

        <!--<input type="submit" name="" value="ОТПРАВИТЬ" class="blue_but2">-->
    <?= Html::submitButton(Yii::t('app','ОТПРАВИТЬ'), ['class' => 'blue_but2', 'name' => 'send-button']) ?>

    <?php ActiveForm::end(); ?>

</div>