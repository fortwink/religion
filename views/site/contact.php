<?
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$this->title = Yii::t('app','Контактные данные');
?>
<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
<?=Yii::t('app','Контакты')?>
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2"><?=Yii::t('app','Заполните заявку и дополните карту новыми данными')?></div>


    </div>
</div>


<div class="white_inf_block_outer">

    <div class="row">

        <div class="col-xs-6 col-sm-3">
            <div class="contacts_block_img"><img src="/images/phone-call.png"></div>
            <div class="contacts_block">
                8 (727) 378-48-06<br>
                8 (727) 378-46-14<br>
                8 (727) 222-17-08<br>
            </div>
        </div>

        <div class="col-xs-6 col-sm-3 ">
            <div class="contacts_block_img"><img src="/images/location.png"></div>
            <div class="contacts_block">
                Юридический адрес:<br>
                пл.Республики 4,<br>
                город Алматы,<br>
                Республика Казахстан,<br>
                050001<br>
            </div>
        </div>

        <div class="col-xs-6 col-sm-3 ">
            <div class="contacts_block_img"><img src="/images/location.png"></div>
            <div class="contacts_block">
                Фактический адрес:<br>
                Толе би 155,<br>
                город Алматы,<br>
                Республика Казахстан,<br>
                050000<br>
            </div>
        </div>

        <div class="col-xs-6 col-sm-3 ">
            <div class="contacts_block_img"><img src="/images/message.png"></div>
            <div class="contacts_block">
                info@ciaudr.kz
            </div>
        </div>
    </div>


    <br><br><br>

    <div style="width: 100%; height: 400px; border:1px solid black; text-align: center;"><iframe src="https://yandex.kz/map-widget/v1/-/CBQ0YCDCcC" width="100%" height="400" frameborder="0"></iframe></div>

    <div class="white_inf_block_outer" style="background-color: #e8e8e8;">
        <div class="blue_form_title"><?=Yii::t('app','Обратная связь')?></div>




        <?php $form = ActiveForm::begin([
            'id' => 'contact-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{input}",
                'options'=>['class'=>''],
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <div class="row">
            <div class="col-md-6"> <?= $form->field($model, 'name')->textInput(['class' => 'blue_form_input ','placeholder'=>Yii::t('app','ФИО')]) ?></div>
            <div class="col-md-6"><?= $form->field($model, 'email')->textInput(['class' => 'blue_form_input','placeholder'=>Yii::t('app','E-mail')]) ?> </div>
        </div>

            <br>

            <?= $form->field($model, 'body')->textarea(['class' => 'blue_form_textarea','placeholder'=>Yii::t('app','Сообщение')]) ?>


            <br><br>

            <?= Html::submitButton(Yii::t('app','ОТПРАВИТЬ'), ['class' => 'blue_but2', 'name' => 'send-button']) ?>

        <?php ActiveForm::end(); ?>


    </div>

</div>