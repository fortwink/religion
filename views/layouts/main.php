<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Lang;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="app">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<?
if (Yii::$app->user->isGuest){
    /* ?><a href="/user/login"><div class="blue_but"><?=Yii::t('app','ВХОД')?></div></a><?*/
} else {
    ?><div  style="background-color: #e8e8e8;"><div class="container"><a class="pull-right" href="/site/logout"><div class="blue_but" style="top:0"><?=Yii::t('app','ВЫХОД')?></div></a><div style="    position: relative;
    top: 9px;">Вы находитесь в режиме авторизации для государственных служащих</div> </div></div><?
}
?>

<!--HEADER -->
<header>
    <div class="header">
        <table width="100%">
            <tr>
                <td width="100px;" class="header_td_padding">
                    <a href="<?=Url::to(['/'])?>"><img class='logo' src="/images/logo.png"></a>
                </td>
                <td class="header_td_padding">
                    <div class="logo_text">
                        <?=Yii::t('app','Управление<br>по делам религий<br>г. Алматы')?>
                        <div class='logo_lang_text'><a class="logo_lang_text_deactive" href="/kz">каз</a> / <a class="logo_lang_text_deactive" href="/">рус</a></div>
                    </div>
                </td>
                <td width="100%"  class="header_td_padding">
                    <div class="header_menu">
                        <div class="header_menu_links" >
                            <a href="<?=Url::to(['/site/about'])?>"><div class="header_menu_item"><?=Yii::t('app','О проекте')?></div></a>
                            <a href="<?=Url::to(['/npa'])?>"><div class="header_menu_item"><?=Yii::t('app','Нормативно<br>правовые акты')?></div></a>
                            <a href="<?=Url::to(['/confession'])?>"><div class="header_menu_item"><?=Yii::t('app','Религия<br> и общество')?></div></a>
                            <a href="<?=Url::to(['/site/faq'])?>"><div class="header_menu_item"><?=Yii::t('app','Справочная<br> информация')?></div></a>
                            <a href="<?=Url::to(['/news'])?>"><div class="header_menu_item"><?=Yii::t('app','Новости<br> и видео')?></div></a>
                            <a href="<?=Url::to(['/site/request'])?>"><div class="header_menu_item"><?=Yii::t('app','Оставить заявку<br> на проверку')?></div></a>
                        </div>
                    </div>
                    <div class="header_menu2">


                        <table width="100%"  class="header_td_padding">
                            <tr>
                                <td class="header_menu2_td1">
                                    <ul class="dropdown">
                                        <li class="dropdown-top">
                                            <a class="dropdown-top" href="#">конфессия &or;</a>
                                            <ul class="dropdown-inside">
                                                <?
                                                    $confess = \app\models\Confession::find()->all();
                                                    foreach ($confess as $con){
                                                        ?>
                                                            <li><a href="<?=Url::to(['/confession/objects','confess'=>$con->id])?>"><?=$con->tname?></a></li>
                                                        <?
                                                    }
                                                ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>

                                <td class="header_menu2_td1" style="text-align: center;" width="90px">
                                    <ul class="dropdown">
                                        <li class="dropdown-top">
                                            <a class="dropdown-top" href="#"><?=Yii::t('app','тип')?>  &or;</a>
                                            <ul class="dropdown-inside">
                                                <?
                                                if (Yii::$app->user->isGuest)
                                                {
                                                    $types = \app\models\Types::find()->where(['active'=>1])->all();
                                                }
                                                else {
                                                    $types = \app\models\Types::find()->all();
                                                }
                                                foreach ($types as $t){
                                                    ?>
                                                    <li><a href="<?=Url::to(['/confession/objects','type'=>$t->id]);?>"><?=$t->tname?></a></li>
                                                    <?
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>

                                <td class="header_menu2_td1" style="text-align: center;">
                                    <ul class="dropdown">
                                        <li class="dropdown-top">
                                            <a class="dropdown-top" href="#"><?=Yii::t('app','район')?>  &or;</a>
                                            <ul class="dropdown-inside">
                                                <?
                                                $regions = \app\models\Region::find()->all();
                                                foreach ($regions as $r){
                                                    ?>
                                                    <li><a href="<?=Url::to(['/confession/objects','region'=>$r->id])?>"><?=$r->tname?></a></li>
                                                    <?
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>

                                <td class="header_menu2_td1" style="text-align: right; padding-right: 20px;">
                                    <form action="<?=Url::to(['/confession/objects'])?>" method="GET">
                                        <input type="text" name="q" size="10">
                                        <button type="submit" class="btn btn-secondary" type="button" style="height: 22px; padding: 0; background-color: white; padding-left:10px; "><span class="glyphicon glyphicon-search"></span></button>
                                    </form>
                                </td>

                                <td class="header_menu2_td2">
                                    <i style="    max-width: 12em;"><?=Yii::t('app','Оповестить о подозрительных лицах')?></i>
                                    <a href="https://api.whatsapp.com/send?phone=77016730911" target="_blank"><img src="/images/whatsapp copy.png"></a>
                                    <a href="https://telegram.me/SerikAkbota"><img src="/images/telegram copy.png"></a>
                                    <a href="viber://add?number=77016730911"><img src="/images/viber copy.png"></a>
                                </td>


                            </tr>
                        </table>


                    </div>

                </td>

                <td style='text-align: right;'>
                    <a href="/#relig_map_bar">
                        <div class="header_rel_map">
                            <img src="/images/r_map.png">
                            <br><br>
                            <?=Yii::t('app','Религиозная карта')?>
                        </div>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</header>

<!--HEADER -->




<!--MOBILE HEADER -->
<div class="mobile_header mobile_show">
    <table width="100%">
        <tr>
            <td class="mobile_header_menu_td">
                <img src="/images/mobile_menu.png"  class="mobile_header_menu" id="mobile_menu">
            </td>
            <td class="mobile_header_logo_td">
                <img src="/images/mobile_logo.png"  class="mobile_header_logo">
            </td>
            <td  class="mobile_header_text">
                <div class="mobile_header_text_div">
                    <?=Yii::t('app','Управление<br>по делам религий<br>г. Алматы')?>
                </div>
            </td>
            <td class="mobile_heade_lang_td">
                <a href="/kz/"><div class="mobile_heade_lang">Каз</div></a>
                <a href="/"><div class="mobile_heade_lang_active">Рус</div></a>
            </td>
            <td class="mobile_header_map_td">
                <a href="/#relig_map_bar">
                    <img src="/images/mobile_map.png">
                </a>

            </td>
        </tr>
        <tr>
            <td colspan="4" class="mobile_header_bar_td">
                <div id="soc_menu">
                     <a href="https://api.whatsapp.com/send?phone=77016730911" target="_blank"><img src="/images/whatsapp copy.png"></a>
                                    <a href="https://telegram.me/SerikAkbota"><img src="/images/telegram copy.png"></a>
                                    <a href="viber://add?number=77016730911"><img src="/images/viber copy.png"></a>
                    <div style="float:left; width: 150px;">
                        <?=Yii::t('app','Оповестить о подозрительных лицах')?>
                    </div>
                </div>

                <div id="search_menu" class="search_menu">




                    <ul class="dropdown">
                        <li class="dropdown-top">
                            <a class="dropdown-top" href="#">конфессия  &or;</a>
                            <ul class="dropdown-inside">
                                <?
                                foreach ($confess as $con){
                                    ?>
                                    <li><a href="/confession/objects?confess=<?=$con->id?>"><?=$con->name?></a></li>
                                    <?
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>

                    <ul class="dropdown">
                        <li class="dropdown-top">
                            <a class="dropdown-top" href="#">тип  &or;</a>
                            <ul class="dropdown-inside">
                                <?
                                foreach ($types as $t){
                                    ?>
                                    <li><a href="/confession/objects?type=<?=$t->id?>"><?=$t->name?></a></li>
                                    <?
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>

                    <ul class="dropdown">
                        <li class="dropdown-top">
                            <a class="dropdown-top" href="#">район  &or;</a>
                            <ul class="dropdown-inside">
                                <?
                                foreach ($regions as $r){
                                    ?>
                                    <li><a href="/confession/objects?region=<?=$r->id?>"><?=$r->name?></a></li>
                                    <?
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>

                </div>
            </td>
            <td style="	border-top: 3px solid #e8e8e8;
	border-bottom: 3px solid #e8e8e8;">
                <div class="mobile_header_bar_search">
                    <img src="/images/search.png" height="30px;">
                </div>
            </td>
        </tr>

    </table>
</div>


<div class="mobile_menu_window">
    <span class="glyphicon glyphicon-remove" id="close_mobile_menu" style="font-size: 35px; padding: 10px; border:2px solid black;"></span>
    <hr>

    <a href="<?=Url::to(['/site/about'])?>"><div class="header_menu_item"><?=Yii::t('app','О проекте')?></div></a>
    <a href="<?=Url::to(['/npa'])?>"><div class="header_menu_item"><?=Yii::t('app','Нормативно<br>правовые акты')?></div></a>
    <a href="<?=Url::to(['/confession'])?>"><div class="header_menu_item"><?=Yii::t('app','Религия<br> и общество')?></div></a>
    <a href="<?=Url::to(['/site/faq'])?>"><div class="header_menu_item"><?=Yii::t('app','Справочная<br> информация')?></div></a>
    <a href="<?=Url::to(['/news'])?>"><div class="header_menu_item"><?=Yii::t('app','Новости<br> и видео')?></div></a>
    <a href="<?=Url::to(['/site/request'])?>"><div class="header_menu_item"><?=Yii::t('app','Оставить заявку<br> на проверку')?></div></a>
</div>



<!--MOBILE HEADER -->
        <?= $content ?>


<!--FOOTER -->
<div class="footers">
    <div class="footer_block">
        <table width="100%">
            <tr>
                <td width="100px;">
                    <img class='logo' src="/images/logo.png">
                </td>
                <td>
                    <div class="logo_text">
                        <?=Yii::t('app','Управление<br>по делам религий<br>г. Алматы')?>
                    </div>
                </td>
                <td width="60%" >

                    <table width="100%" class="footer_table">
                        <tr>







                            <td><a href="<?=Url::to(['/site/about'])?>"><div class="header_menu_item"><?=Yii::t('app','О проекте')?></div></a></td>
                            <td><a href="<?=Url::to(['/npa'])?>"><div class="header_menu_item"><?=Yii::t('app','Нормативно<br>правовые акты')?></div></a></td>
                            <td><a href="<?=Url::to(['/confession'])?>"><div class="header_menu_item"><?=Yii::t('app','Религия<br> и общество')?></div></a></td>
                            <td><a href="<?if (Lang::getCurrent()->url != 'ru') echo '/'.Lang::getCurrent()->url; ?>/#relig_map_bar"><div class="header_menu_item"><?=Yii::t('app','Религиозная<br> карта')?></div></a></td>
                        </tr>
                        <tr>
                            <td><a href="<?=Url::to(['/site/faq'])?>"><div class="header_menu_item"><?=Yii::t('app','Справочная<br> информация')?></div></a></td>
                            <td><a href="<?=Url::to(['/news'])?>"><div class="header_menu_item"><?=Yii::t('app','Новости<br> и видео')?></div></a></td>
                            <td><a href="<?=Url::to(['/site/request'])?>"><div class="header_menu_item"><?=Yii::t('app','Оставить заявку<br> на проверку')?></div></a></td>
                            <td><a href="<?=Url::to(['/site/contact'])?>"><div class="header_menu_item"><?=Yii::t('app','Контакты')?></div></a></td>
                        </tr>
                    </table>


                </td>

                <td>
                    <img src="/images/location2.png" style="float:right;">
                </td>

                <td style='text-align: right;'>
                    <div class="footer_adress">
                        <p>+ 7 (727) 378-46-14</p>
                    <p><?=Yii::t('app','г. Алматы, ул. Толе-би 155, оф.607, 6 этаж')?>
                        <a href="mailto:info@religionmap.kz">info@religionmap.kz</a></p>
                    </div>
                </td>
            </tr>
        </table>
    </div>



    <div class="footer_soc clearfix">

        <?
            if (Yii::$app->user->isGuest){
               /* ?><a href="/user/login"><div class="blue_but"><?=Yii::t('app','ВХОД')?></div></a><?*/
            } else {
                ?>Вы находитесь в режиме авторизации для государственных служащих <a href="/site/logout"><div class="blue_but"><?=Yii::t('app','ВЫХОД')?></div></a><?
            }
/*
            <div class="blue_text"><i><?=Yii::t('app','Вход для сотрудников государственных учреждений')?></i></div>

        */?>

        <div class="footer_soc_right">

            <i><?=Yii::t('app','Оповестить о подозрительных лицах')?></i>
            <a href="https://api.whatsapp.com/send?phone=77016730911" target="_blank"><img src="/images/whatsapp copy.png"></a>
                                    <a href="https://telegram.me/SerikAkbota"><img src="/images/telegram copy.png"></a>
                                    <a href="viber://add?number=77016730911"><img src="/images/viber copy.png"></a>

        </div>
    </div>

</div>




<div class="mobile_footer mobile_show">
    <table width="100%">
        <tr>
            <td width="180px">
                <img class='logo' src="/images/logo.png">
                <br><br>
            </td>
            <td colspan="2">
                <br><br>
                <div class="footer_adress" style="float:right; padding-right: 20px; ">
                    <p>+ 7 (727) 378-46-14</p>
                    <p><?=Yii::t('app','г. Алматы, ул. Толе-би 155, (бывшее здание БТИ) оф.607, 6 этаж')?>
                        <a href="mailto:info@religionmap.kz">info@religionmap.kz</a></p>
                    <br>
                </div>
            </td>
        </tr>
        <tr >
            <td class="mobile_footer_bar_td" style="padding-top: 30px;">
                <?
                if (Yii::$app->user->isGuest){
                   /* ?><a href="/user/login"><div class="blue_but"><?=Yii::t('app','ВХОД')?></div></a><?*/
                } else {
                    ?><a href="/site/logout"><div class="blue_but"><?=Yii::t('app','ВЫХОД')?></div></a><?
                }?>
            </td>

            <td class="mobile_footer_bar_td" style="text-align: left">
                <div class="blue_text"><i><? //=Yii::t('app','Вход для сотрудников государственных учреждений')?></i></div>
            </td>

            <td class="mobile_footer_bar_td">
                <a href="#"><span class="glyphicon glyphicon-chevron-up" style="font-size: 35px; margin: 0 auto; "></span></a>
            </td>
        </tr>
    </table>

</div>




<!--FOOTER -->

<?php $this->endBody() ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-85370743-3', 'auto');
    ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45703692 = new Ya.Metrika({
                    id:45703692,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45703692" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
<?php $this->endPage() ?>
