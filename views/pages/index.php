<?php
/* @var $this yii\web\View */

$this->title = $model->ttitle;
?>
<div class="container">
    <h1><?=$model->ttitle?></h1>
    <div>
        <?
            if (Yii::$app->user->id && $model->ttext_hidden)
                echo $model->ttext_hidden;
            else
                echo $model->ttext;

        ?>
    </div>
</div>
