<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Управление
            по делам религий
            г. Алматы ',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Миссионеры', 'url' => ['/admin/people']],
            ['label' => 'Новости', 'url' => ['/admin/news']],
            ['label' => 'Объекты', 'url' => ['/admin/objects']],
            ['label'=>'Справочники','items'=>[

                ['label' => 'Нормативно-правовые акты', 'url' => ['/admin/npa']],
                ['label' => 'Категории нормативно-правовых актов', 'url' => ['/admin/npacat']],
                ['label' => 'Партнеры', 'url' => ['/admin/partners']],
                ['label' => 'Конфессии', 'url' => ['/admin/confession']],
                ['label' => 'Христианские конфессии', 'url' => ['/admin/subconfess']],
                ['label' => 'Типы объектов', 'url' => ['/admin/types']],
                ['label' => 'Справочно-правовая информация', 'url' => ['/admin/spi']],
                ['label' => 'Страницы', 'url' => ['/admin/pages']],
                ['label' => 'Регионы', 'url' => ['/admin/region']],
                ['label' => 'Сниппеты', 'url' => ['/admin/snippets']],

            ]],
            ['label' => 'User', 'url' => ['/user/admin']],
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/user/login']] : // or ['/user/login-email']
                ['label' => 'Logout (' . Yii::$app->user->displayName . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']],
           // ['label' => 'Религия и общество', 'url' => ['/site/about']],
           // ['label' => 'Справочная информация', 'url' => ['/site/about']],
           // ['label' => 'Новости и видео', 'url' => ['/site/contact']],
           // ['label' => 'Оставить заявку на проверку ', 'url' => ['/site/contact']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
