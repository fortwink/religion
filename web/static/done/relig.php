<?php

include("header.php");
?>


<div class="main relig_top_fix">

    <br><br>

    <div class="relig_header"></div>
    <br><br><br>
    <img src="images/islam_ic.png" class="relig_icon">

    <div class="relig_desc">

        <div class="green_h1 relig_desc_title"><b>Ислам</b></div>
        <br>

        <div class="relig_sub_title">Очень краткая информация о конфессии</div>
        <br>

        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>

        <div class="relig_hide_desc">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>


        <br>

        <div class="relig_grey_but" id="show_more"><i>Более детальное описание </i>&or;</div>
    </div>

    <br><br><br><br>

    <div class="relig_grey_inf_block_outer">

        <div class="blue_h1">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            Культовые сооружения
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            <br>
        </div>


        <div class="relig_eg_block">
            <br><br>

            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="relig_eg" style="background-image: url(images/cult1.png);"></div>
                    <div class="relig_text">ЗИсламское Религиозное
                        объединение мечеть
                        "Султан Корган"
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="relig_eg" style="background-image: url(images/cult2.png);"></div>
                    <div class="relig_text">
                        Религиозное объединение
                        "Организация обучения
                        и знания священного Корана"
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="relig_eg" style="background-image: url(images/cult3.png);"></div>
                    <div class="relig_text">Религиозное объединение
                        "Хуссейния"
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="relig_eg" style="background-image: url(images/cult4.png);"></div>
                    <div class="relig_text">
                        Филиал Республиканского
                        исламского религиозного
                        объединения"Духовное управление
                        мусульман Казахстана"
                        "Мечеть поселка Дружба"
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blue_but2" style="position: relative; top:-25px; font-size: 20px;">Перйти в раздел</div>


    <div class="white_inf_block_outer">
        <div class="green_h1 relig_desc_title">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            Миссионеры
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>

        <div class="relig_missianers_outer"></div>

        <div class="relig_eg_block">
            <br><br>

            <div class="row">
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
            </div>

            <br>

        </div>


        <div class="green_but2" style="position: relative; top:0px; font-size: 20px;">Перйти в раздел</div>

        <br><br><br>


        <div class="green_inf_block_outer">
            <div class="white_h1">
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
                Стационарные помещения
                <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            </div>
            <br><br>

            <div class="relig_green_block_inner">

                <div class="row">
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                    <div class="col-xs-6 col-sm-6 relig_green_block_item"><img src="images/stat_ic.png">

                        <p>
                            «Джагамбек и С» жауапкерлігі
                            шектеулі сеіріктестігі, «Гулянда» кітаптар үй</p></div>
                </div>

            </div>
        </div>


    </div>
    </div>


    <script type="text/javascript">

        $("#show_more").click(function () {


            if ($(".relig_hide_desc").is(":hidden")) {
                $(".relig_hide_desc").show("slow");
                $("#show_more").html("<i>Скрыть </i>&and;");
            } else {
                $(".relig_hide_desc").hide("slow");
                $("#show_more").html("<i>Более детальное описание </i>&or;");
            }


        });

    </script>


    <?php

    include("footer.php");
    ?>


    </body>
    </html>