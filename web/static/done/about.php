<?php

include("header.php");
?>


<br><br>
<div class="main">
    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        О проекте
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2">Cервис, который позволяет вам найти и узнать больше информации
            о религиозных объектах города Алматы
        </div>
        <br>

    </div>
</div>


<div class="p_eg_block">
    <div class="row">
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg1"></div>
            <div class="p_eg_text">Здесь вы можете найти
                официальный перечень
                конфессий
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg2"></div>
            <div class="p_eg_text">На карте указаны все
                культовые сооружения
                с подробной информацией
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg3"></div>
            <div class="p_eg_text">Информация
                о миссионерах
                и их деятельности
                на территории РК
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 ">
            <div class="p_eg4"></div>
            <div class="p_eg_text">Собрана база данных
                религиозных помещений
                различного назначения
            </div>
        </div>
    </div>
</div>


<div class="grey_inf_block_outer">
    <div class="grey_inf_block_inner">
        <div class="grey_inf_block_h1">НА ПОРТАЛЕ</div>
        <br>

        <p>Описание целей проекта интерактивных карт.</p>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur.</p>

        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
    </div>

</div>


<div class="white_inf_block_outer">

    <div class="green_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Ситуация в городе
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="grey_inf_block_inner" style="padding-bottom: 0;">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur.</p>
    </div>


    <div class="reg_map">
        <div class="aria_map_1" id="r1">Жетысуйский</div>
        <div class="aria_map_2" id="r2">Алатауский</div>
        <div class="aria_map_3" id="r3">Ауэзовский</div>
        <div class="aria_map_4" id="r4">Наурызбайский</div>
        <div class="aria_map_5" id="r5">Бостандыкский</div>
        <div class="aria_map_6" id="r6">Турскибский</div>
        <div class="aria_map_7" id="r7">Алмалинский</div>
        <div class="aria_map_8" id="r8">Медеуский</div>


        <img class="reg_map_img" id="rimage" src="images/about_map/jetisu.png">


        <div class="reg_map_text">
            <div class="aria_map_1_text">
                <div class="aria_map_text_title">1%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_2_text">
                <div class="aria_map_text_title">2%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_3_text">
                <div class="aria_map_text_title">3%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_4_text">
                <div class="aria_map_text_title">4%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_5_text">
                <div class="aria_map_text_title">5%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>

        <div class="reg_map_text">
            <div class="aria_map_6_text">
                <div class="aria_map_text_title">6%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_7_text">
                <div class="aria_map_text_title">7%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>


        <div class="reg_map_text">
            <div class="aria_map_8_text">
                <div class="aria_map_text_title">8%</div>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                ad minim veniam, quis nostrud exercitation ullamco laboris aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit.
            </div>
        </div>
    </div>


</div>



<div class="green_inf_block_outer">


    <div class="white_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Поддержка проекта
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>
    <br><br>

    <div class="green_inf_block_inner">
        <p color="green"><font color="#007705">«Центр изучения и анализа» Управления по делам религий города
                Алматы.</font></p>

        <p>Предметом деятельности Центра является осуществление научных исследований в сфере религии.
            Целью деятельности Центра является оценка и анализ религиозной ситуации в городе Алматы, а также проведение
            разъяснительных работ.</p>
    </div>
    <br>
    <a href="http://www.ciaudr.kz/about/">www.ciaudr.kz/about/</a>
</div>




<div class="white_inf_block_outer">

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Если возникли вопросы
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br class="mobile_hide"><br class="mobile_hide">
    </div>
    <br><br>


    <div class="quation_block_1">
        По любым вопросам
        вы можете воспользоваться
        данными контактами
    </div>

    <div class="quation_block_2">
        <img src="images/location.png"> г. Алматы, ул. Толе-би 155, (бывшее здание БТИ) оф.607, 6 этаж<br><br>
        <img src="images/phone-call.png"> +7 (727) 378-46-14, +7 (727) 378-48-06<br><br>
        <img src="images/message.png"> centr.ddralmaty@mail.ru<br>
    </div>


</div>


<?php

include("footer.php");
?>


<script type="text/javascript">


    $("#r1").click(function () {
        $("#rimage").attr("src", "images/about_map/jetisu.png");

        $(".aria_map_1").css("color", "#e40424");
        $(".aria_map_1_text").css("opacity", "1");

        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");


        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");

    });


    $("#r2").click(function () {
        $("#rimage").attr("src", "images/about_map/alatau.png");

        $(".aria_map_2").css("color", "#e40424");
        $(".aria_map_2_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");

    });

    $("#r3").click(function () {
        $("#rimage").attr("src", "images/about_map/auez.png");

        $(".aria_map_3").css("color", "#e40424");
        $(".aria_map_3_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");

    });


    $("#r4").click(function () {
        $("#rimage").attr("src", "images/about_map/nauriz.png");

        $(".aria_map_4").css("color", "#e40424");
        $(".aria_map_4_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");

    });

    $("#r5").click(function () {
        $("#rimage").attr("src", "images/about_map/bostan.png");

        $(".aria_map_5").css("color", "#e40424");
        $(".aria_map_5_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");

    });

    $("#r6").click(function () {
        $("#rimage").attr("src", "images/about_map/turk.png");

        $(".aria_map_6").css("color", "#e40424");
        $(".aria_map_6_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");
    });

    $("#r7").click(function () {
        $("#rimage").attr("src", "images/about_map/almalin.png");

        $(".aria_map_7").css("color", "#e40424");
        $(".aria_map_7_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_8").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_8_text").css("opacity", "0");
    });

    $("#r8").click(function () {
        $("#rimage").attr("src", "images/about_map/medeo.png");

        $(".aria_map_8").css("color", "#e40424");
        $(".aria_map_8_text").css("opacity", "1");

        $(".aria_map_1").css("color", "#484848");
        $(".aria_map_2").css("color", "#484848");
        $(".aria_map_3").css("color", "#484848");
        $(".aria_map_4").css("color", "#484848");
        $(".aria_map_5").css("color", "#484848");
        $(".aria_map_6").css("color", "#484848");
        $(".aria_map_7").css("color", "#484848");

        $(".aria_map_1_text").css("opacity", "0");
        $(".aria_map_2_text").css("opacity", "0");
        $(".aria_map_3_text").css("opacity", "0");
        $(".aria_map_4_text").css("opacity", "0");
        $(".aria_map_5_text").css("opacity", "0");
        $(".aria_map_6_text").css("opacity", "0");
        $(".aria_map_7_text").css("opacity", "0");
    });


</script>


</body>
</html>