<?php

include("header.php");
?>


<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Миссионеры
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
    </div>
    <br>

    <div class="missioner_block">
        <div class="missioner_photo">

            <div class="relig_missianers_eg_big" style="background-image: url('images/missioner.png');">
                <img src="/images/islam_ic.png">
            </div>

        </div>
        <div class="missioner_text_block">
            <br><br>

            <div class="missioner_green_font1">
                <?=$model->name?>
            </div>
            <br><br>

            <div class="missioner_black_font2">
                Дата регистрации:
                <div class="missioner_green_font2">
                    22.04.1965
                </div>
            </div>
            <br><br>

            <div class="missioner_black_font2">
                Гражданство: Россия<br>
                Место регистрации: Казахстан
            </div>
            <br><br>

            <div class="missioner_black_font1">
                <i><b>Ислам</b></i>
            </div>
            <br><br>

            <div class="missioner_black_font1">
                <img src="images/phone-call.png"> 263-77-22,8701-511-72-78
            </div>
            <br><br>

            <div class="missioner_green_font2">
                Филиал Республиканскiого исламского религиозного объединения
                "Духовное управление мусульман Казахстана" мечеть "Бекет ата"
            </div>
            <br><br>

            <div class="missioner_black_font2">
                Дата регистрации:
                <div class="missioner_green_font2">
                    2.04.2000
                </div>
                <br>
                Дата перерегистрации:
                <div class="missioner_green_font2">
                    22.04.2015
                </div>
            </div>
        </div>
    </div>


    <br><br><br>

    <div style="background-color: #e8e8e8;">
        <div class="missioner_grey_block">
            <div class="missioner_grey_block_item">
                Статус: положительный
                <br><br>
                Номер свидетельства о гос.регистрации: 690395321 986
                <br><br>
                Ориентировочные сборы: 1 200 000 тг/ мес
            </div>

            <div class="missioner_grey_block_item">
                Статус: положительный
                <br><br>
                Номер свидетельства о гос.регистрации: 690395321 986
                <br><br>
                Ориентировочные сборы: 1 200 000 тг/ мес
            </div>
        </div>
    </div>

</div>

<?php

include("footer.php");
?>


</body>
</html>