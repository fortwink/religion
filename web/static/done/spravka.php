<?php

include("header.php");
?>


<div class="main">
<br>
<br>

	<div class="red_h1">
		<div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div> 
			Справочная информация
		<div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
		<br>	
		<div class="black_h2" style="width: 500px; margin: 0 auto; padding: 10px;">Очень краткая информация о разделе,
возможно в две строки</div>


	</div>
</div>
<br>

<div class="docs_body">

<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 1</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>


<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 2</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p> 2 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>
</div>


<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 3</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p> 3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>
</div>


<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 4</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p>4 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	</div>
</div>


<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 5</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p> 5 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>
</div>


<div class="spravka_block" id=""><span><div class="green_circl"><b>&#8226;</b></div> Текст справки 6</span> <img src="images/arr_down.png" style="float:right;">
	<div class="spravka_block_content">
		<p> 6 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

	</div>
</div>




</div>


</div>
<br><br><br> 


<script type="text/javascript">
	



$(".spravka_block").click(function() {

if($(this).children(".spravka_block_content").is(':hidden')) {
  $(this).children(".spravka_block_content").show("slow");
  $(this).children("img").attr("src","images/arr_up.png");
  $(this).children("span").css("color","red");
  $(this).children("span").children(".green_circl").css("color","red");



}else{
  
  $(this).children(".spravka_block_content").hide("slow");
  $(this).children("img").attr("src","images/arr_down.png");
  $(this).children("span").css("color","#484848");
   $(this).children("span").children(".green_circl").css("color","green");

}


});




</script>



<?php

include("footer.php");
?>


</body>
</html>