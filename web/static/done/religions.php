<?php

include("header.php");
?>


<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Религия и общество
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2">Очень краткая информация о разделе,
            возможно в две строки
        </div>
    </div>
    <br><br>

    <div class="religions_grey_block_top_icon_outer">
        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="images/icon_hrist.png">

                <div class="religions_icon_text" style="color:#69c1d3">ХРИСТИАНСТВО</div>
            </div>
        </div>
    </div>


    <div class="religions_grey_block_outer">
        <div class="religions_grey_block_inner">


            <div class="religions_grey_block_inner_links">
                Заголовок 1<br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>

            </div>


            <div class="religions_grey_block_inner_links">
                Заголовок 2<br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>


            </div>


            <div class="religions_grey_block_inner_links">
                Заголовок 3<br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>
                <a href="">Ссылка с длинным анкором</a><br>

            </div>


            <div class="religions_grey_block_inner_links">
                <img src="images/blue_rumb.png" width="15px">
                <a href="">Другая ссылка с ромбом</a><br>

                <img src="images/blue_rumb.png" width="15px">
                <a href="">Другая ссылка с ромбом</a><br>

                <img src="images/blue_rumb.png" width="15px">
                <a href="">Другая ссылка с ромбом</a><br>

            </div>


            <br><br><br>
        </div>
    </div>


    <br>

    <div class="religions_grey_block_bottom_icon_outer">

        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="images/icon_islam.png">

                <div class="religions_icon_text" style="color:#52ac62; ">ИСЛАМ</div>
            </div>
        </div>


        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="images/icon_buddizm.png">

                <div class="religions_icon_text" style="color:#e6d80a; ">БУДДИЗМ</div>
            </div>
        </div>

        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="images/icon_bahai.png">

                <div class="religions_icon_text" style="color:#8582bc; ">БАХАИ</div>
            </div>
        </div>

        <div class="religions_icon_inline">
            <div class="religions_icon_block">
                <img src="images/icon_iud.png">

                <div class="religions_icon_text" style="color:#3b3b3b; ">ИУДЕЙСКАЯ <br> РЕЛИГИЯ</div>
            </div>
        </div>

        <div class="religions_icon_inline religions_icon_block_mob_fix">
            <div class="religions_icon_block">
                <img src="images/icon_krishna.png">

                <div class="religions_icon_text" style="color:#e8a765; ">ОБЩЕСТВО <br> СОЗНАНИЯ КРИШНЫ</div>
            </div>
        </div>

    </div>
</div>


<?php

include("footer.php");
?>


</body>
</html>