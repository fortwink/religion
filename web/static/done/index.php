<?php

include("header.php");
?>


<div class="main">


    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Религиозная карта
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>

        <div class="black_h2" style="margin: 0 auto; padding: 10px;">Cервис, который позволяет вам найти и узнать больше
            информации
            о религиозных объектах города Алматы
        </div>
    </div>

    <br><br>

    <div class="index_green_block">

        <div class="row">
            <div class="col-lg-6 col-xs-12 ">

                <div class="index_green_block_item">
                    <div class="index_green_block_item_text">официальный
                        перчень конфессий
                    </div>
                </div>
                <div class="index_green_block_item">
                    <div class="index_green_block_item_text">культовые сооружения
                        с подробной
                        информацией
                    </div>
                </div>
                <div class="index_green_block_item">
                    <div class="index_green_block_item_text">миссионеры
                        и их деятельность
                        на территории РК
                    </div>
                </div>
                <div class="index_green_block_item">
                    <div class="index_green_block_item_text">база данных
                        религиозных помещений
                        различного назначения
                    </div>
                </div>

            </div>


            <div class="col-lg-6 col-xs-12">

                <img src="images/green_background_img.png" class="mobile_hide" style="margin:0 auto;">
            </div>


        </div>
    </div>


    <br><br>

    <div class="white_inf_block_outer">

        <div class="index_black_h1">
            <div class="h1_rombs"><font color="#69c1d3">&#9830; &#9830; &#9830; &#9830;</font></div>
            Вы можете начать пользоваться
            <font color="#69c1d3">информационным сервисом</font> прямо сейчас
            <div class="h1_rombs"><font color="#69c1d3">&#9830; &#9830; &#9830; &#9830;</font></div>
        </div>

        <br>
    </div>


    <div class="relig_map_bar">
        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#">конфессия &or;</a>
                    <ul class="dropdown-inside">
                        <li><a href="/">Подкатегория 1</a></li>
                        <li><a href="/">Подкатегория 2</a></li>
                        <li><a href="/">Подкатегория 3</a></li>
                        <li><a href="/">Подкатегория 4</a></li>
                        <li><a href="/">Подкатегория 5</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#">тип &or;</a>
                    <ul class="dropdown-inside">
                        <li><a href="/">Подкатегория 1</a></li>
                        <li><a href="/">Подкатегория 2</a></li>
                        <li><a href="/">Подкатегория 3</a></li>
                        <li><a href="/">Подкатегория 4</a></li>
                        <li><a href="/">Подкатегория 5</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="relig_map_bar_item">
            <ul class="dropdown">
                <li class="dropdown-top">
                    <a class="dropdown-top" href="#">район &or;</a>
                    <ul class="dropdown-inside">
                        <li><a href="/">Подкатегория 1</a></li>
                        <li><a href="/">Подкатегория 2</a></li>
                        <li><a href="/">Подкатегория 3</a></li>
                        <li><a href="/">Подкатегория 4</a></li>
                        <li><a href="/">Подкатегория 5</a></li>
                    </ul>
                </li>
            </ul>
        </div>


        <div class="relig_map_bar_form mobile_hide">
            <form action="index.php" method="POST" style="display: inline-block;">
                <input type="" name="" size="16" style="border:none; height: 27px">
                <button type="submit" class="btn btn-secondary" type="button"
                        style="height: 27px; padding: 0; background-color: #e8e8e8;  "><span
                        class="glyphicon glyphicon-search"></span></button>
            </form>
            <div class="relig_map_bar_form_white_but">весь список</div>
        </div>
    </div>

    <div style="width: 100%; height: 400px; border:1px solid black; text-align: center;">место под карту</div>

    <div class="white_inf_block_outer">

        <div class="blue_but2" style="position: relative; top:-65px; font-size: 20px;">Перйти в раздел</div>

        <br>

        <div class="index_inf_block center_align_mobile"><img src="images/index_comp.png"></div>
        <div class="index_inf_block center_align_mobile">
            <div class="index_black_h1">
                Достоверная информация
                <br>
                <font color="#007705">&#9830; &#9830; &#9830; &#9830;</font>
            </div>
            <p>Нам важно, чтобы вы получали только актуальные данные, поэтому
                в случаеiii, если вы увидели некорректные данные, или отсутствующий в базе объект - просьба обратиться к
                нам, и наши специалисты свяжутся в вами и обязательно проверят достоверность данных
                или идентифицируют новый объект.</p>

            <div class="index_green_but">оставить заявку</div>
        </div>


    </div>


    <div class="green_inf_block_outer">


        <div class="white_h1">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            Религия и общество
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>
        <br><br>

        <div class="green_inf_block_inner">
            <p color="green"><font color="#007705">Многообразие - не есть недостаток, наоборот, это самый неоценимый дар
                    Божий, благодаря которому происходит настоящее взаимообогащение, подлинное развитие. Каждый народ
                    обладает неповторимыми, присущими только ему традициями, историей, складом мышления. Каждый вносит
                    свою особую мелодию в полифинию культуры человечества.” (с)</font></p>

            <p>Выступление Президента РК Н.А.Назарбаева на закрытии I Съезда лидров мировых и традиционных религий</p>
        </div>
        <br>

        <div class="index_green_reg_icon">
            <img src="images/islam.png">
            <br>
            <br>
            <a href="">Ислам</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="images/bible.png">
            <br>
            <br>
            <a href="">Хрестианство</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="images/krest.png" height="105px;">
            <br>
            <br>
            <a href="">Протестанты</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="images/bahai.png">
            <br>
            <br>
            <a href="">Бахаи</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="images/om.png">
            <br>
            <br>
            <a href="">Буддизм</a>
        </div>


        <div class="index_green_reg_icon">
            <img src="images/star-of-david.png">
            <br>
            <br>
            <a href="">Иудаизм</a>
        </div>


    </div>
</div>


<div style="text-align: center;">

    <div class="index_grey_inf_block">
        <div class="index_grey_inf_block_big_font"><b>Нормативно-правовые стандарты</b></div>
        <br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>

        <div class="index_grey_inf_block_small_font">Больше документов и информации доступно внутри раздела</div>
        <div class="blue_but2" style="position: relative; top:45px; margin: 0 auto;">Перйти в раздел</div>
    </div>

    <div class="index_grey_inf_block">
        <div class="index_grey_inf_block_big_font"><b>Нормативно-правовые стандарты</b></div>
        <br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>
        <img src="images/blue_rumb.png">
        <a href="">Стандарты стационарных помещений</a>
        <br><br>

        <div class="index_grey_inf_block_small_font">Больше документов и информации доступно внутри раздела</div>
        <div class="blue_but2" style="position: relative; top:45px; margin: 0 auto;">Перйти в раздел</div>
    </div>


</div>

<br><br>

<div class="white_inf_block_outer">

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Видео о проекте
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
    </div>

    <br><br>

    <div class="video_aria">

        <div class="video-responsive">
            <iframe width="auto" height="auto" src="https://www.youtube.com/embed/UcsA-o6nKlg" frameborder="0"
                    allowfullscreen></iframe>
        </div>

    </div>


    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
        }

        .slider {
            width: 90%;
            margin: 20px auto;
        }

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
            color: green;
        }
    </style>

    <br><br><br>

    <div class="green_h1">Партнёры</div>

    <div style="background-color: #e8e8e8; padding: 20px; margin-top: 40px;">

        <section class="center slider">
            <div>
                <img src="http://placehold.it/350x150?text=1">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=2">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=3">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=4">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=5">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=6">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=7">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=8">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=9">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=10">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=11">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=12">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=13">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=14">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=15">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=16">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=17">
            </div>
            <div>
                <img src="http://placehold.it/350x150?text=9">
            </div>

        </section>

    </div>
</div>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).on('ready', function () {

        $(".center").slick({
            dots: false,
            infinite: true,
            centerMode: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });

    });
</script>

<?php
include("footer.php");
?>


</body>
</html>