<?php

include("header.php");
?>


<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Новости и видео
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>

    </div>
    <br><br>

    <div class="news_menu">
        <a href="" style="color: #e40424; text-decoration: underline;">Новости</a>
        <a href="">Видео</a>
        <a href="">Весь список</a>
    </div>
    <br>

    <div class="news_block">


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <div class="news_item">
            <div class="news_item_image_div" style="background-image: url(images/1.jpg);"></div>
            <div class="news_item_title"><a href="news_page.php">Название видео длинное
                    в две строки</a></div>
            <div class="news_item_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam.
                quis nostrud exercitation ullamco laboris nisi.
            </div>
            <div class="news_item_more"><i><a href="news_page.php">Подробнее>></a></i></div>
        </div>


        <br><br>
        <br><br>

        <div class="pagination_block">
            <a href=""><</a>

            <a href="">1</a>
            <a href="" class="pagination_active">2</a>
            <a href="">3</a>
            ...
            <a href="">8</a>

            <a href="">></a>

        </div>
        <br><br><br>


    </div>
</div>

    <?php

    include("footer.php");
    ?>


    </body>
    </html>