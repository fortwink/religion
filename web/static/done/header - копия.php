<!DOCTYPE html>
<html>
<head>
	<title>Религиозная карта Алматы</title>
	<meta charset="utf-8" name="viewport"
          content="initial-scale=0.7,maximum-scale=0.7, width=device-width,  user-scalable=no"/>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/about_map.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./slick/slick.css">
  	<link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
  		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

</head>

</head>
<body>

<!--HEADER -->
<header>
<div class="header">
<table width="100%">
	<tr>
		<td width="100px;" class="header_td_padding">
			<a href="index.php"><img class='logo' src="images/logo.png"></a>
		</td>
		<td class="header_td_padding">
			<div class="logo_text">
				Управление<br>
				по делам религий<br>
				г. Алматы
				<div class='logo_lang_text'><a class="logo_lang_text_deactive" href="">каз</a> / рус</div>	
			</div>
		</td>
		<td width="100%"  class="header_td_padding">
			<div class="header_menu">
				<div class="header_menu_links" >
					<a href="about.php"><div class="header_menu_item">О проекте</div></a>
					<a href="docs.php"><div class="header_menu_item">Нормативно<br>правовые акты</div></a>
					<a href="relig.php"><div class="header_menu_item">Религия<br> и общество</div></a>
					<a href="spravka.php"><div class="header_menu_item">Справочная<br> информация</div></a>
					<a href="news.php"><div class="header_menu_item">Новости<br> и видео</div></a>
					<a href="zayavka.php"><div class="header_menu_item">Оставить заявку<br> на проверку</div></a>
				</div>
			</div>
			<div class="header_menu2">


		<table width="100%"  class="header_td_padding">
			<tr>
			<td class="header_menu2_td1">	
			<div class="header_menu_links header_menu2_links">
					<div class="header_menu_item">
						
					<ul class="dropdown">
					<li class="dropdown-top">
						<a class="dropdown-top" href="#">конфессия  &or;</a>
						<ul class="dropdown-inside">
							<li><a href="/">Подкатегория 1</a></li>
							<li><a href="/">Подкатегория 2</a></li>
							<li><a href="/">Подкатегория длинное название 3</a></li>
							<li><a href="/">Подкатегория 4</a></li>
							<li><a href="/">Подкатегория 5</a></li>
							<li><a href="/">Подкатегория 6</a></li>
							<li><a href="/">Подкатегория 7</a></li>
						</ul>
					</li>
						
					</div>
					<a href=""><div class="header_menu_item">тип &or;</div></a>
					<a href=""><div class="header_menu_item">район &or;</div></a>

					<div class="input-group" style="width:120px;">
				
				      <input type="text" class="form-control" placeholder="" style="height: 22px;">
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" type="button" style="height: 22px; padding: 0; background-color: white; padding-left:10px; "><span class="glyphicon glyphicon-search"></span></button>
				      </span>
				     

				    </div>
					
			</div>
			
			</td>

			<td class="header_menu2_td2">
				<i>Оповестить<br> о подозрительных лицах</i>
					<img src="images/whatsapp copy.png">
					<img src="images/telegram copy.png">
					<img src="images/viber copy.png">		
			</td>
			</table>
				

			</div>
			
		</td>

		<td style='text-align: right;'>
			<div class="header_rel_map">
				<img src="images/r_map.png">
				<br><br>
				Религиозная карта
			</div>
		</td>
	</tr>
</table>	
</div>
</header>

<!--HEADER -->




<!--MOBILE HEADER -->
<div class="mobile_header mobile_show">
	<table width="100%">
		<tr>
			<td class="mobile_header_menu_td">
				<img src="images/mobile_menu.png"  class="mobile_header_menu" id="mobile_menu">
			</td>
			<td class="mobile_header_logo_td">
				<img src="images/mobile_logo.png"  class="mobile_header_logo">
			</td>
			<td  class="mobile_header_text">
				<div class="mobile_header_text_div">
					Управление
					по делам религий
					г. Алматы
				</div>	
			</td>
			<td class="mobile_heade_lang_td">
				<div class="mobile_heade_lang">Каз</div>
				<div class="mobile_heade_lang_active">Рус</div>
			</td>
			<td class="mobile_header_map_td">
				<img src="images/mobile_map.png">		
			</td>
		</tr>
		<tr>
			<td colspan="4" class="mobile_header_bar_td">
				<div id="soc_menu">	
					<img src="images/whatsapp copy.png">	
					<img src="images/telegram copy.png">
					<img src="images/viber copy.png">
						<div style="float:left; width: 150px;">		
								Оповестить о подозрительных лицах			
						</div>	
				</div>

				<div id="search_menu" class="search_menu">	




					<ul class="dropdown">
					<li class="dropdown-top">
						<a class="dropdown-top" href="#">конфессия  &or;</a>
						<ul class="dropdown-inside">
							<li><a href="/">Подкатегория 1</a></li>
							<li><a href="/">Подкатегория 2</a></li>
							<li><a href="/">Подкатегория длинное название 3</a></li>
							<li><a href="/">Подкатегория 4</a></li>
							<li><a href="/">Подкатегория 5</a></li>
							<li><a href="/">Подкатегория 6</a></li>
							<li><a href="/">Подкатегория 7</a></li>
						</ul>
					</li>

					<ul class="dropdown">
					<li class="dropdown-top">
						<a class="dropdown-top" href="#">тип  &or;</a>
						<ul class="dropdown-inside">
							<li><a href="/">Подкатегория 1</a></li>
							<li><a href="/">Подкатегория 2</a></li>
							<li><a href="/">Подкатегория длинное название 3</a></li>
							<li><a href="/">Подкатегория 4</a></li>
							<li><a href="/">Подкатегория 5</a></li>
						</ul>
					</li>

					<ul class="dropdown">
					<li class="dropdown-top">
						<a class="dropdown-top" href="#">район  &or;</a>
						<ul class="dropdown-inside">
							<li><a href="/">Подкатегория 1</a></li>
							<li><a href="/">Подкатегория 2</a></li>
							<li><a href="/">Подкатегория длинное название 3</a></li>
							<li><a href="/">Подкатегория 4</a></li>
							<li><a href="/">Подкатегория 5</a></li>
						</ul>
					</li>


				</div>		
			</td>
			<td style="	border-top: 3px solid #e8e8e8;
	border-bottom: 3px solid #e8e8e8;">
				<div class="mobile_header_bar_search">
					<img src="images/search.png" height="30px;">
				</div>
			</td>
		</tr>	

	</table>
</div>


<div class="mobile_menu_window">
<span class="glyphicon glyphicon-remove" id="close_mobile_menu" style="font-size: 35px; padding: 10px; border:2px solid black;"></span>
	<hr>
	<a href="">Ссылка 1</a><br>
	<a href="">Ссылка 2</a><br>
	<a href="">Ссылка 3</a><br>
	<a href="">Ссылка 4</a><br>
	<a href="">Ссылка 5</a><br>
	<a href="">Ссылка 6</a><br>
	<a href="">Ссылка 7</a><br>
</div>


<script type="text/javascript">

$(".mobile_header_bar_search").click(function() {

if($("#soc_menu").is(':hidden')){
  $("#soc_menu").show();
  $("#search_menu").hide();
  $(".mobile_header_bar_td").css("background-color","#e8e8e8");	
}else{
  $(".mobile_header_bar_td").css("background-color","white");
  $("#soc_menu").hide();
  $("#search_menu").show();
  }
});


$("#mobile_menu").click(function() {
	$(".mobile_menu_window").show();
	$("body").css("overflow-y","hidden");
});


$("#close_mobile_menu").click(function() {
	$(".mobile_menu_window").hide();
	$("body").css("overflow-y","auto");
});

</script>
<!--MOBILE HEADER -->

