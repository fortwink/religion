

<!--FOOTER -->
<div class="footers">
<div class="footer_block">
<table width="100%">
	<tr>
		<td width="100px;">
			<img class='logo' src="images/logo.png">
		</td>
		<td>
			<div class="logo_text">
				Управление<br>
				по делам религий<br>
				г. Алматы
			</div>
		</td>
		<td width="60%" >
	
		<table width="100%" class="footer_table">
			<tr>
				<td><a href="about.php"><div class="header_menu_item">О проекте</div></a></td>
				<td><a href="docs.php"><div class="header_menu_item">Нормативно<br>правовые акты</div></a></td>
				<td><a href=""><div class="header_menu_item">Религия<br> и общество</div></a></td>
				<td><a href=""><div class="header_menu_item">Религиозная<br> карта</div></a></td>
			</tr>
			<tr>
				<td><a href="spravka.php"><div class="header_menu_item">Справочная<br> информация</div></a></td>
				<td><a href=""><div class="header_menu_item">Новости<br> и видео</div></a></td>
				<td><a href=""><div class="header_menu_item">Оставить заявку<br> на проверку</div></a></td>
				<td><a href="contacts.php"><div class="header_menu_item">Контакты</div></a></td>
			</tr>
		</table>
		
			
		</td>

		<td>
			<img src="images/location2.png" style="float:right;">
		</td>

		<td style='text-align: right;'>
			<div class="footer_adress">
			<p>+ 7 (777) 123 45 56</p>
			<p>г. Алматы, ул.Абая, 123/оф3
			adress@gmail.com</p>
			</div>
		</td>
	</tr>
</table>	
</div>



<div class="footer_soc">

	<div class="blue_but">ВХОД</div>
	
	<div class="blue_text"><i>Вход для сотрудников
государственных учреждений</i></div>


<div class="footer_soc_right">
	
<i>Оповестить<br> о подозрительных лицах</i>
<img src="images/whatsapp copy.png">
<img src="images/telegram copy.png">
<img src="images/viber copy.png">		

</div>
</div>

</div>




<div class="mobile_footer mobile_show">
	<table width="100%">
		<tr>
			<td width="180px">
				<img class='logo' src="images/logo.png">
				<br><br>
			</td>
			<td colspan="2">
			<br><br>
				<div class="footer_adress" style="float:right; padding-right: 20px; ">
				<p>+ 7 (777) 123 45 56</p>
				<p>г. Алматы, ул.Абая, 123/оф3
				adress@gmail.com</p>
				<br>
			</div>
			</td>
		</tr>
		<tr >
			<td class="mobile_footer_bar_td" style="padding-top: 30px;">
				<div class="blue_but">ВХОД</div>
			</td>
			
			<td class="mobile_footer_bar_td" style="text-align: left">	
				<div class="blue_text"><i>Вход для сотрудников
государственных учреждений</i></div>
			</td>
				
				<td class="mobile_footer_bar_td">
				<a href="#"><span class="glyphicon glyphicon-chevron-up" style="font-size: 35px; margin: 0 auto; "></span></a>
			</td>
		</tr>
	</table>

</div>




<!--FOOTER -->
