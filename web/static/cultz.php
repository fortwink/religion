<?php

include("header.php");
?>


<div class="main">
    <br>
    <br>

    <div class="red_h1">
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        Культовые сооружения
        <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        <br>
    </div>
    <br><br><br>

    <div class="zdan_block">
        <div class="zdan_photo">
            <div class="zdan_zdan_photo" style="background-image: url('../images/1.jpg');"></div>
            <img src="images/islam_ic.png">
        </div>
        <div class="zdan_text_block">
            <div class="zdan_green_font1">

                Филиал Республиканского исламского религиозного
                объединения "Духовное управление мусульман Казахстана"
                мечеть "Бекет ата"
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <i><b>ИСЛАМ</b></i>
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <img src="images/location.png"> 050061, ГОРОД АЛМАТЫ, АЛАТАУСКИЙ РАЙОН, МИКРОРАЙОН ШАНЫРАК 1, УЛИЦА
                ОТЕМИС УЛЫ, д. 109
                <div class="zdan_blue_font1"><a href="0"><i>посмотреть на карте</i></div>
                </a>
            </div>
            <br><br>

            <div class="zdan_black_font1">
                <img src="images/phone-call.png"> 263-77-22, 8701-511-72-78
            </div>
            <br><br>

            <div class="zdan_black_font2">
                Количество прихожан:

                <div class="zdan_blue_font2"><b>1520</b></div>
                человек

                <br><br>


                Дата регистрации:
                <div class="zdan_green_font2">
                    2.04.2000
                </div>
                <br>
                Дата перерегистрации:
                <div class="zdan_green_font2">
                    22.04.2015
                </div>
            </div>
        </div>
    </div>


    <br><br><br>

    <div style="background-color: #e8e8e8;">
        <div class="zdan_grey_block">
            <div class="zdan_grey_block_item">
                Статус: положительный
                <br><br>
                Номер свидетельства о гос.регистрации: 690395321 986
                <br><br>
                Ориентировочные сборы: 1 200 000 тг/ мес
            </div>

            <div class="zdan_grey_block_item">
                Статус: положительный
                <br><br>
                Номер свидетельства о гос.регистрации: 690395321 986
                <br><br>
                Ориентировочные сборы: 1 200 000 тг/ мес
            </div>
        </div>
    </div>


    <div class="zdan_white_block">
        <div class="zdan_black_font1">
            <i><b>Описание культового объекта</b></i>
        </div>
        <br><br>

        <div class="zdan_black_font2">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id dsad dsadas dasd est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>


    <style type="text/css">


        .slider {
            width: 90%;
            margin: 0px auto;
            text-align: center;
        }

        .slick-slide {
            width: 100%;
            padding: 5px;
        }

        .slick-slide img {
            width: 100%;

        }

        .slick-prev:before,
        .slick-next:before {
            color: green;

        }

        }

        @media (max-width: 1000px) {
            .slider {
                width: 100%;
            }


    </style>


    <div class="zdan_white_block" style="margin-top: -30px;">
        <section class="center slider">
            <div>
                <img
                    src="https://image.shutterstock.com/display_pic_with_logo/595873/529187599/stock-photo-night-view-taxco-city-in-mexico-529187599.jpg">
            </div>
            <div>
                <img
                    src="https://image.shutterstock.com/display_pic_with_logo/650461/526658740/stock-photo-whale-shark-rhincodon-typus-the-biggest-fish-in-the-ocean-a-huge-gentle-plankton-filterer-giant-526658740.jpg">
            </div>
            <div>
                <img
                    src="https://image.shutterstock.com/display_pic_with_logo/2980894/366339032/stock-photo-monarch-butterflies-on-tree-branch-in-blue-sky-background-michoacan-mexico-366339032.jpg">
            </div>

            <div>
                <img
                    src="https://image.shutterstock.com/display_pic_with_logo/1054852/107461973/stock-photo-volcano-and-church-107461973.jpg">
            </div>
            <div>
                <img
                    src="https://image.shutterstock.com/display_pic_with_logo/615949/328869371/stock-photo-sweet-bread-called-bread-of-the-dead-pan-de-muerto-enjoyed-during-day-of-the-dead-festivities-in-328869371.jpg">
            </div>
        </section>
    </div>


    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).on('ready', function () {

            $(".center").slick({
                dots: false,
                infinite: true,
                centerMode: true,
                slidesToShow: 2,
                slidesToScroll: 2
            });

        });
    </script>


    <br><br><br>

    <div class="grey_inf_block_outer">
        <div class="green_h1 relig_desc_title">
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
            Священнослужители
            <div class="h1_rombs">&#9830; &#9830; &#9830; &#9830;</div>
        </div>

        <div class="relig_missianers_outer"></div>

        <div class="relig_eg_block">
            <br><br>

            <div class="row">
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/man.png');"></div>
                    <div class="relig_text">Ким Жиннам</div>
                </div>
                <div class="col-xs-6 col-sm-2">
                    <div class="relig_missianers_eg" style="background-image: url('images/woman.png');"></div>
                    <div class="relig_text">Надольный
                        Воьфганг
                    </div>
                </div>
            </div>
            <br>
        </div>


    </div>
</div>

<?php

include("footer.php");
?>


</body>
</html>