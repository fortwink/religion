/**
 * Created by Victor on 28.06.2017.
 */


'use strict';
(function ($, ng) {

    var app = ng.module('app', ['ngAnimate','ymaps']);
    app.config(['$httpProvider', '$compileProvider', '$sceDelegateProvider','ymapsConfig', function ($httpProvider, $compileProvider, $sceDelegateProvider,ymapsConfig) {
        ymapsConfig.clusterize = false;
        ymapsConfig.mapBehaviors = ['drag'];
       // ymapsConfig.mapBehaviors.scrollZoom = false;


        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://vk.com/**'
        ]);

        $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
            return {
                'request': function (config) {
                    $rootScope.$broadcast('loading-started');
                    return config || $q.when(config);
                },
                'response': function (response) {
                    $rootScope.$broadcast('loading-complete');
                    return response || $q.when(response);
                }
            };
        }]);
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.transformRequest = [function (data) {
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subValue, innerObj, i;

                for (name in obj) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value instanceof Object) {
                        for (var subName in value) {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
    }]);








    app.controller('objectspage', ['$scope', '$http', '$timeout', '$sce', "$attrs", function ($scope,$http,$timeout,$sce,$attrs ) {
        $scope.showmap = true;
    }]);

    app.controller('objectdetail', ['$scope', '$http', '$timeout', '$sce', "$attrs", function ($scope,$http,$timeout,$sce,$attrs ) {
        $scope.seeonmap = false;
    }]);

    app.controller('confessview', ['$scope', '$http', '$timeout', '$sce', "$attrs", function ($scope,$http,$timeout,$sce,$attrs ) {
        $scope.detail1 = false;
    }]);
    app.controller('mainpage', ['$scope', '$http', '$timeout', '$sce', "$attrs", function ($scope,$http,$timeout,$sce,$attrs ) {


        $scope.map = {
            center: [43.253596, 76.902197], zoom: 10
        };


        $scope.region = '';
        $scope.confess = '';
        $scope.type = '';
        $scope.q = '';
        var lang_check = window.location.pathname.substr(1).indexOf('kz')==0;
        var lang_suff = (lang_check)?'_kz':'';
        var lang_pref = (lang_check)?'/kz':'';


        $scope.setConfess = function(c){
            $scope.confess = c;
            var f = '?confess_id='+$scope.confess;
            if ($scope.region) {
                f += '&region_id='+$scope.region;
            }
            if ($scope.q) {
                f += '&q='+$scope.q;
            }
            if ($scope.type) {
                f += '&type_id='+$scope.type;
            }
            $http.get('/site/getloc'+f).then(function (response) {
                $scope.markers = [];
                ng.forEach(response.data,function (item) {
                    if (item.map && item.map.indexOf(',')>0) {
                        $scope.markers.push({
                            coordinates: item.map.split(','),
                            properties: {
                                balloonContent:
                                '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                                '<i>'+item.confession+'</i><br>'+
                                item.address+'<br>'+
                                '<a href="'+lang_pref+'/confession/objectview?id='+item.id+'">Подробнее>></a>'
                            },
                            options: {
                                iconLayout: 'default#image',
                                iconImageHref:  item.iconpic,
                                iconImageSize: [40, 40],
                                iconImageOffset: [-3, -42]
                            }
                        });
                    }
                });
            },function (response) {
            });

        };
        $scope.setQ = function(){
            // $scope.confess = c;
            var f = '?q='+$scope.q;
            if ($scope.confess) {
                f += '&confess_id='+$scope.confess;
            }
            if ($scope.region) {
                f += '&region_id='+$scope.region;
            }
            if ($scope.type) {
                f += '&type_id='+$scope.type;
            }
            $http.get('/site/getloc'+f).then(function (response) {
                $scope.markers = [];
                ng.forEach(response.data,function (item) {
                    if (item.map && item.map.indexOf(',')>0) {
                        $scope.markers.push({
                            coordinates: item.map.split(','),
                            properties: {
                                balloonContent:
                                '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                                '<i>'+item.confession+'</i><br>'+
                                item.address+'<br>'+
                                '<a href="'+lang_pref+'/confession/objectview?id='+item.id+'">Подробнее>></a>'
                            },
                            options: {
                                iconLayout: 'default#image',
                                iconImageHref:  item.iconpic,
                                iconImageSize: [40, 40],
                                iconImageOffset: [-3, -42]
                            }
                        });
                    }
                });
            },function (response) {
            });

        };
        $scope.setRegion = function(c){
            $scope.region = c;
            var f = '?region_id='+$scope.region;
            if ($scope.confess) {
                f += '&confess_id='+$scope.confess;
            }
            if ($scope.q) {
                f += '&q='+$scope.q;
            }
            if ($scope.type) {
                f += '&type_id='+$scope.type;
            }
            $http.get('/site/getloc'+f).then(function (response) {
                $scope.markers = [];

                ng.forEach(response.data,function (item) {
                    if (item.map && item.map.indexOf(',')>0) {
                        $scope.markers.push({
                            coordinates: item.map.split(','),
                            properties: {
                                balloonContent:
                                '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                                '<i>'+item.confession+'</i><br>'+
                                item.address+'<br>'+
                                '<a href="'+lang_pref+'/confession/objectview?id='+item.id+'">Подробнее>></a>'
                            },
                            options: {
                                iconLayout: 'default#image',
                                iconImageHref:  item.iconpic,
                                iconImageSize: [40, 40],
                                iconImageOffset: [-3, -42]
                            }
                        });
                    }
                });
            },function (response) {
            });
        };

        $scope.setType = function(c){
            $scope.type = c;
            var f = '?type_id='+$scope.type;
            if ($scope.region) {
                f += '&region_id='+$scope.region;
            }
            if ($scope.q) {
                f += '&q='+$scope.q;
            }
            if ($scope.confess) {
                f += '&confess_id='+$scope.confess;
            }
            $http.get('/site/getloc'+f).then(function (response) {
                $scope.markers = []

                ng.forEach(response.data,function (item) {
                    if (item.map && item.map.indexOf(',')>0) {
                        $scope.markers.push({
                            coordinates: item.map.split(','),
                            properties: {
                                balloonContent:
                                '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                                '<i>'+item.confession+'</i><br>'+
                                item.address+'<br>'+
                                '<a href="'+lang_pref+'/confession/objectview?id='+item.id+'">Подробнее>></a>'
                            },
                            options: {
                                iconLayout: 'default#image',
                                iconImageHref: item.iconpic,
                                iconImageSize: [60, 60],
                                iconImageOffset: [-3, -42]
                            }
                        });
                    }
                });
            },function (response) {
            });
        };



        $scope.filters = [];

        $http.get('/site/getfilters').then(function(response){
            $scope.filters = response.data;
        },function(){});

        $http.get('/site/getloc/').then(function (response) {
            $scope.markers = []

            ng.forEach(response.data,function (item) {
                if (item.map && item.map.indexOf(',')>0) {
                    $scope.markers.push({
                        coordinates: item.map.split(','),
                        properties: {
                            balloonContent:
                            '<img style="float:right;" src="'+item.image+'">'+'<b>'+item.short_name+'</b><br>'+
                            '<i>'+item.confession+'</i><br>'+
                            item.address+'<br>'+
                            '<a href="'+lang_pref+'/confession/objectview?id='+item.id+'">Подробнее>></a>'
                        },
                        options: {
                            iconLayout: 'default#image',
                            iconImageHref:  item.iconpic,
                            iconImageSize: [40, 40],
                            iconImageOffset: [-3, -42]
                        }
                    });
                }
            });
        },function (response) {
        });

        $scope.markers = [
            /*{coordinates:[43.253596, 76.102197], properties: {balloonContent: 'Здесь рыбы нет!'}},
            {coordinates:[43.253596, 76.902197], properties: {balloonContent: 'Здесь рыбы тоже нет'}, options: {preset: 'islands#icon', iconColor: '#a5260a'}},
            {coordinates:[43.253596, 76.402197], properties: {balloonContent: 'А здесь есть!'}}*/
        ];

    }]);


    $(".center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });

    $(".mobile_header_bar_search").click(function() {

        if($("#soc_menu").is(':hidden')){
            $("#soc_menu").show();
            $("#search_menu").hide();
            $(".mobile_header_bar_td").css("background-color","#e8e8e8");
        }else{
            $(".mobile_header_bar_td").css("background-color","white");
            $("#soc_menu").hide();
            $("#search_menu").show();
        }
    });


    $("#mobile_menu").click(function() {
        $(".mobile_menu_window").show();
        $("body").css("overflow-y","hidden");
    });


    $("#close_mobile_menu").click(function() {
        $(".mobile_menu_window").hide();
        $("body").css("overflow-y","auto");
    });


    $(".spravka_block").click(function() {

        if($(this).children(".spravka_block_content").is(':hidden')) {
            $(this).children(".spravka_block_content").show("slow");
            $(this).children("img").attr("src","/images/arr_up.png");
            $(this).children("span").css("color","red");
            $(this).children("span").children(".green_circl").css("color","red");



        }else{

            $(this).children(".spravka_block_content").hide("slow");
            $(this).children("img").attr("src","/images/arr_down.png");
            $(this).children("span").css("color","#484848");
            $(this).children("span").children(".green_circl").css("color","green");

        }


    });

    $("#show_more").click(function () {


        /*if ($(".relig_hide_desc").is(":hidden")) {
            $(".relig_hide_desc").show("slow");
            $("#show_more").html("<i>Скрыть </i>&and;");
        } else {
            $(".relig_hide_desc").hide("slow");
            $("#show_more").html("<i>Более детальное описание </i>&or;");
        }*/


    });

    $(".center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 2,
        slidesToScroll: 2
    });

    $('body').on('click','a.dropdown-top',function(e){
        e.preventDefault();
        console.log('clicked')
    });


    /*ymaps.ready(function () {
        var myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10
        });
    });*/


})(jQuery, angular);





