<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        //'css/about_map.css',
        //'css/style.css',
        //'slick/slick.css',
        //'slick/slick-theme.css',

    ];
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'vendor/bower/angular/angular.min.js',
        'vendor/bower/angular-ymaps/angular-ymaps.js',
        //'slick/slick.js',
        'js/app.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
