<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 17/07/17
 * Time: 11:13
 */

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;


class AccessController extends Controller {


    public function init()
    {
        if (!empty(Yii::$app->user) && !Yii::$app->user->can("admin")) {
            Yii::$app->user->setReturnUrl('/admin');
            $this->redirect('/user/login');
        }
        parent::init();
    }

}
