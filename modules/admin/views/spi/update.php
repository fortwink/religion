<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Spi */

$this->title = 'Update Spi: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Spis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="spi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
