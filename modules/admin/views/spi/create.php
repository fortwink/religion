<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Spi */

$this->title = 'Create Spi';
$this->params['breadcrumbs'][] = ['label' => 'Spis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
