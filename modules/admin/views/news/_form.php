<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'active')->checkbox() ?>
    
    <?= $form->field($model, 'type')->dropDownList([1=>'Новость', 2=>'Видео']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_kz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'preview_text')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'preview_text_kz')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'detail_text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'detail_text')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?= $form->field($model, 'detail_text_kz')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?/* $this->registerJs("CKEDITOR.plugins.addExternal('filebrowser', '/ckeditor/filebrowser/plugin.js', '');");?>
    <? $this->registerJs("CKEDITOR.plugins.addExternal('popup', '/ckeditor/popup/plugin.js', '');");?>
    <?= $form->field($model, 'detail_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'custom',
        'clientOptions' => [
            'extraPlugins' => 'filebrowser,popup',
            'toolbarGroups' => [
                ['name' => 'undo'],
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'colors'],
                ['name' => 'links', 'groups' => ['links', 'insert']],
                ['name' => 'others', 'groups' => ['others', 'about']],
                ['name' => 'filebrowser'] // <--- OUR NEW PLUGIN YAY!
            ]
        ]
    ])*/ ?>



    <?= $form->field($model, 'picture')->fileInput() ?>


    <?
        $image = $model->getImage();
        echo Html::img('/'.$image->getPath('100x75'));
    ?>

    <?//= $form->field($model, 'date')->textInput() ?>
    <?= $form->field($model, 'date')->widget(
        DatePicker::className(), [
        // inline too, not bad
        'inline' => true,
        // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy.mm.dd'
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
