<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Npacat */

$this->title = 'Update Npacat: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Npacats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="npacat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
