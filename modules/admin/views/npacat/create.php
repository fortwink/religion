<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Npacat */

$this->title = 'Create Npacat';
$this->params['breadcrumbs'][] = ['label' => 'Npacats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="npacat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
