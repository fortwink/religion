<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Npa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="npa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\search\Npacat::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'file')->fileInput() ?>
    <?=$model->file?>
    <?= $form->field($model, 'file_kz')->fileInput() ?>
    <?=$model->file_kz?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
