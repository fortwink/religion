<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Npa */

$this->title = 'Update Npa: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Npas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="npa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
