<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Npa */

$this->title = 'Create Npa';
$this->params['breadcrumbs'][] = ['label' => 'Npas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="npa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
