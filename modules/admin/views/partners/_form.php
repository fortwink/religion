<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

/**
 * @var yii\web\View $this
 * @var app\models\Partners $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

            'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Name...', 'maxlength' => 255]],

            'image' => ['type' => TabularForm::INPUT_FILE, 'options' => ['placeholder' => 'Enter ...']],



        ]

    ]);

    echo Html::img('/'.$model->getImage()->getPath('300x150'));
    ?>
    <br>
    <hr>
    <br>

    <?


    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
