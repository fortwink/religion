<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Confession */

$this->title = 'Create Confession';
$this->params['breadcrumbs'][] = ['label' => 'Confessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="confession-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
