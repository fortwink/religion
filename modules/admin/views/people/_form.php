<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\People */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="people-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'confess_id')->dropDownList(ArrayHelper::map(\app\models\Confession::find()->all(),'id','name'),['prompt'=>"Выберите конфессию"]); ?>
    <?= $form->field($model, 'subconfess_id')->dropDownList(ArrayHelper::map(\app\models\Subconfess::find()->all(),'id','name'),['prompt'=>"Выберите ответвление христианства"]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'gr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bdate')->textInput() ?>

    <?//= $form->field($model, 'register')->textarea() ?>
    <?= $form->field($model, 'register')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>


    <?//= $form->field($model, 'description')->textarea() ?>
    <?= $form->field($model, 'description')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>


    <?= $form->field($model, 'image')->fileInput() ?>

    <?
    $image = $model->getImage();
    echo Html::img('/'.$image->getPath('100x100'));
    ?>

    <?= $form->field($model, 'exp')->textInput() ?>

    <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'object_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\models\Objects::find()->all(),'id','name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите объект'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    // ->dropDownList(ArrayHelper::map(\app\models\Objects::find()->all(),'id','name'),['prompt'=>"Выберите объект"]); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
