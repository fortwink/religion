<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Objectssearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Objects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Objects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'header' => 'Скрыт',
                'attribute'=>'status',
                'filter' => [
                    0=>'Нет',
                    1=>'Да'
                ],
                'value' => function($model){
                    return ($model->status)?'Да':'Нет';
                }
            ],
            //'status',
            'name',
            'short_name',
            'confess_id',
            'address',

            [
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img('/'.$model->getImage()->getPath('100x100'));
                }
            ],
            // 'contacts',
            // 'region_id',
            // 'description:ntext',
            // 'preview_text:ntext',
            // 'hidden_text:ntext',
            // 'missioners:ntext',
            'map',
            // 'type_id',
            // 'style_id',
            // 'icon_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
