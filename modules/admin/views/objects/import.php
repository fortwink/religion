<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 02.07.2017
 * Time: 23:44
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>


<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'file')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>




