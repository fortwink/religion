<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Types;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
//use \msvdev\widgets\mappicker\MapInput;
use \msvdev\widgets\mappicker\MapInput;

use kartik\file\FileInput;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Objects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'confess_id')->dropDownList(ArrayHelper::map(\app\models\Confession::find()->all(),'id','name'),['prompt'=>"Выберите конфессию"]); ?>
    <?= $form->field($model, 'subconfess_id')->dropDownList(ArrayHelper::map(\app\models\Subconfess::find()->all(),'id','name'),['prompt'=>"Выберите "]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'short_name_kz')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(\app\models\Region::find()->all(),'id','name'),['prompt'=>"Выберите район"]); ?>

    <?= $form->field($model, 'contacts')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'map')->textInput();
   /* echo $form->field($model, 'map')->widget('kolyunya\yii2\widgets\MapInputWidget',[
        'key'=>'AIzaSyAiLbjnl6rlqdGlwK4fsWQ5c8WhCIZVMQw',
        'longitude'=>76.8512485,
        'latitude' =>43.2220146,
        'zoom'=>12,
        'pattern' => '%latitude%,%longitude%',
    ]);*/
    ?>

    <?
        echo $form->field($model, 'map')->widget(
            MapInput::className(),
            [
                'language' => 'ru-RU', // map language, default is the same as in the app
                'service' => 'yandex', // map service provider, "google" or "yandex", default "google"
                //'apiKey' => '', // required google maps
                //'coordinatesDelimiter' => ',', // attribute coordinate string delimiter, default "@" (lat@lng)
                'mapWidth' => '800px', // width map container, default "500px"
                'mapHeight' => '500px', // height map container, default "500px"
                'mapZoom' => '14', // map zoom value, default "10"
                'mapCenter' => [43.308129342096706,76.881422996521], // coordinates center map with an empty attribute, default Moscow
            ]
        );
    ?>





    <? //= $form->field($model, 'preview_text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'preview_text')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?= $form->field($model, 'preview_text_kz')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?
    $images = $model->images;
    $img_arr = [];
    $config = [];
    foreach ($images as $id=>$image) {
        $img_arr[] = '/'.$image->getPath('150x120');
        $config[] = [
            'url'=>'/admin/objects/fileupload',
            'key'=>$image->id,
        ];
    }
    ?>

    <?= $form->field($model, 'preview_pic[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
            'multiple'=>true
        ],
        'pluginOptions' => [
            'initialPreview'=>$img_arr,
            'initialPreviewAsData'=>true,
            'uploadUrl' => Url::to(['/admin/objects/fileupload']),
            'showRemove' => true,
            'showUpload' => false,
            'overwriteInitial'=>true,
            'initialPreviewConfig'=>$config
        ]

    ]);?>


    <?//= $form->field($model, 'hidden_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hidden_text')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?= $form->field($model, 'hidden_text_kz')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>


    <? //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'description')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?><?= $form->field($model, 'description_kz')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?= $form->field($model, 'missioners')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(Types::find()->all(),'id','name'),['prompt'=>"Выберите тип"]); ?>

    <?= $form->field($model, 'style_id')->textInput() ?>

    <?= $form->field($model, 'icon_id')->dropDownList(ArrayHelper::map(\app\models\Icons::find()->all(),'id','name'),['prompt'=>'Выберите']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    document.addEventListener("DOMContentLoaded", function(){
        $('#objects-preview_pic').on('filesorted', function(event, params) {
            //console.log('File sorted ', params.previewId, params.oldIndex, params.newIndex, params.stack);
            console.log(params.stack);
            $.post(
                '/admin/objects/filesort',
                {
                    data:params.stack
                },
                function (data) {
                    console.log(data);
                }
            );
        });
    });


</script>

<style>
    .kv-file-upload {
        display: none;
    }
</style>