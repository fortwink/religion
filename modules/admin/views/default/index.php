<div class="admin-default-index">
    <h1>Добро пожаловать в административный раздел сайта Religionmap.kz</h1>
    <p>
       Для начала работы выберите раздел. Например: <a href="/admin/people">Миссионеры</a> или <a href="/admin/news">Новости</a>
    </p>
</div>
