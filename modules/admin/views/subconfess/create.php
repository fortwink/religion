<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subconfess */

$this->title = 'Create Subconfess';
$this->params['breadcrumbs'][] = ['label' => 'Subconfesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subconfess-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
