<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;


/* @var $this yii\web\View */
/* @var $model app\models\Subconfess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subconfess-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'protest')->checkbox() ?>

    <?= $form->field($model, 'short_text')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'short_text_kz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_text')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>
<?= $form->field($model, 'full_text_kz')->widget(TinyMce::className(),[
        'fileManager' => [
            'class' => TinyMceElFinder::className(),
            'connectorRoute' => 'el-finder/connector',
        ],
    ]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>
    <?
        $image = $model->getImage();
        echo Html::img('/'.$image->getPath('100x75'));
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
