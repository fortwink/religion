<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function init()
    {
        if (\Yii::$app->user->isGuest){
            // header('Location: /user/login');
            //$this->redirect
        }
        parent::init();


        // custom initialization code goes here
    }
}
