<?php

use yii\db\Migration;

/**
 * Handles adding title to table `news`.
 */
class m170711_180728_add_title_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'title', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'title');
    }
}
