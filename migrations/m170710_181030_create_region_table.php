<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `region`.
 */
class m170710_181030_create_region_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name'=>Schema::TYPE_STRING

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('region');
    }
}
