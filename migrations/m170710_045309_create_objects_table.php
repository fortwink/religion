<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `objects`.
 */
class m170710_045309_create_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('objects', [
            'id' => $this->primaryKey(),
            'name'=>Schema::TYPE_STRING,
            'short_name'=>Schema::TYPE_STRING,
            'confess_id'=>Schema::TYPE_INTEGER,
            'address'=>Schema::TYPE_STRING,
            'contacts'=>Schema::TYPE_STRING,
            'region_id'=>Schema::TYPE_INTEGER,
            'type_id'=>Schema::TYPE_INTEGER,
            'style_id'=>Schema::TYPE_INTEGER,
            'icon_id'=>Schema::TYPE_INTEGER,
            'description'=>Schema::TYPE_TEXT,
            'preview_text'=>Schema::TYPE_TEXT,
            'hidden_text'=>Schema::TYPE_TEXT,
            'missioners'=>Schema::TYPE_STRING,/*create many to many */
            'map'=>Schema::TYPE_STRING,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('objects');
    }
}
