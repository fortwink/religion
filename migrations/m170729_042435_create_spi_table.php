<?php

use yii\db\Migration;

/**
 * Handles the creation of table `spi`.
 */
class m170729_042435_create_spi_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('spi', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'title_kz' => $this->string(),
            'text' => $this->text(),
            'text_kz' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('spi');
    }
}
