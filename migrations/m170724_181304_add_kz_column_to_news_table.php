<?php

use yii\db\Migration;

/**
 * Handles adding kz to table `news`.
 */
class m170724_181304_add_kz_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'title_kz', $this->string());
        $this->addColumn('news', 'preview_text_kz', $this->string());
        $this->addColumn('news', 'detail_text_kz', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'title_kz');
        $this->dropColumn('news', 'preview_text_kz');
        $this->dropColumn('news', 'detail_text_kz');
    }
}
