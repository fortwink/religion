<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `confession`.
 */
class m170711_105201_add_sort_column_to_confession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('confession', 'sort', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('confession', 'sort');
    }
}
