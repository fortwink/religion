<?php

use yii\db\Migration;

/**
 * Handles adding subconfess_id to table `people`.
 */
class m170723_043347_add_subconfess_id_column_to_people_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('people', 'subconfess_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('people', 'subconfess_id');
    }
}
