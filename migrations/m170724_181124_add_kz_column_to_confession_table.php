<?php

use yii\db\Migration;

/**
 * Handles adding kz to table `confession`.
 */
class m170724_181124_add_kz_column_to_confession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('confession', 'name_kz', $this->string());
        $this->addColumn('confession', 'short_text_kz', $this->string());
        $this->addColumn('confession', 'full_text_kz', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('confession', 'name_kz');
        $this->dropColumn('confession', 'short_text_kz');
        $this->dropColumn('confession', 'full_text_kz');
    }
}
