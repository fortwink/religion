<?php

use yii\db\Migration;

/**
 * Handles the creation of table `npa_cat`.
 */
class m170720_165332_create_npa_cat_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('npa_cat', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('npa_cat');
    }
}
