<?php

use yii\db\Migration;

/**
 * Handles adding kz to table `objects`.
 */
class m170724_181440_add_kz_column_to_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('objects', 'name_kz', $this->string());
        $this->addColumn('objects', 'short_name_kz', $this->string());
        $this->addColumn('objects', 'description_kz', $this->text());
        $this->addColumn('objects', 'preview_text_kz', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('objects', 'name_kz');
        $this->dropColumn('objects', 'short_name_kz');
        $this->dropColumn('objects', 'description_kz');
        $this->dropColumn('objects', 'preview_text_kz');
    }
}
