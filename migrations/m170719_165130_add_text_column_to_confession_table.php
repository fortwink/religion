<?php

use yii\db\Migration;

/**
 * Handles adding text to table `confession`.
 */
class m170719_165130_add_text_column_to_confession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('confession', 'short_text', $this->string());
        $this->addColumn('confession', 'full_text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('confession', 'short_text');
        $this->dropColumn('confession', 'full_text');
    }
}
