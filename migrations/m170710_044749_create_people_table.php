<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `people`.
 */
class m170710_044749_create_people_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('people', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING,
            'bdate'=> Schema::TYPE_INTEGER,
            'gr'=> Schema::TYPE_STRING,
            'register'=>Schema::TYPE_STRING,
            'confess_id'=>Schema::TYPE_INTEGER,
            'contact_phone'=>Schema::TYPE_STRING,
            'object_id'=>Schema::TYPE_INTEGER,
            'register_dates'=>Schema::TYPE_STRING,
            'hidden_info'=>Schema::TYPE_TEXT

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('people');
    }
}
