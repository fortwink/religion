<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m170729_154441_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'title_kz' => $this->string(),
            'text' => $this->text(),
            'text_kz' => $this->text(),
            'sef' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pages');
    }
}
