<?php

use yii\db\Migration;

/**
 * Handles adding name to table `npa`.
 */
class m170803_102842_add_name_column_to_npa_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('npa', 'name_kz', $this->string());
        $this->addColumn('npa', 'file_kz', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('npa', 'name_kz');
        $this->dropColumn('npa', 'file_kz');
    }
}
