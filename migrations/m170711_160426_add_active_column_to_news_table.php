<?php

use yii\db\Migration;

/**
 * Handles adding active to table `news`.
 */
class m170711_160426_add_active_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'active', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'active');
    }
}
