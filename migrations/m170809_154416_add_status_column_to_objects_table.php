<?php

use yii\db\Migration;

/**
 * Handles adding status to table `objects`.
 */
class m170809_154416_add_status_column_to_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('objects', 'status', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('objects', 'status');
    }
}
