<?php

use yii\db\Migration;

/**
 * Handles adding type to table `news`.
 */
class m170711_180852_add_type_column_to_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('news', 'type', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('news', 'type');
    }
}
