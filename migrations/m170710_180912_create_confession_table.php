<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `confession`.
 */
class m170710_180912_create_confession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('confession', [
            'id' => $this->primaryKey(),
            'name'=>Schema::TYPE_STRING

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('confession');
    }
}
