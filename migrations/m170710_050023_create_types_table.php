<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `types`.
 */
class m170710_050023_create_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('types', [
            'id' => $this->primaryKey(),
            'name'=>Schema::TYPE_STRING
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('types');
    }
}
