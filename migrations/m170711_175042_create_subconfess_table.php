<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subconfess`.
 */
class m170711_175042_create_subconfess_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subconfess', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'protest' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subconfess');
    }
}
