<?php

use yii\db\Migration;

/**
 * Handles adding subconfess_id to table `objects`.
 */
class m170711_175809_add_subconfess_id_column_to_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('objects', 'subconfess_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('objects', 'subconfess_id');
    }
}
