<?php

use yii\db\Migration;

/**
 * Handles the creation of table `snippets`.
 */
class m170809_114812_create_snippets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('snippets', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'text' => $this->text(),
            'text_kz' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('snippets');
    }
}
