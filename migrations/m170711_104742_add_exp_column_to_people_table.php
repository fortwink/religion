<?php

use yii\db\Migration;

/**
 * Handles adding exp to table `people`.
 */
class m170711_104742_add_exp_column_to_people_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('people', 'exp', $this->integer());
        $this->addColumn('people', 'position', $this->string());
        $this->addColumn('people', 'bplace', $this->string());
        $this->addColumn('people', 'description', $this->text());
        $this->addColumn('people', 'nation', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('people', 'exp');
        $this->dropColumn('people', 'position');
        $this->dropColumn('people', 'bplace');
        $this->dropColumn('people', 'description');
        $this->dropColumn('people', 'nation');
    }
}
