<?php

use yii\db\Migration;

/**
 * Handles adding file to table `npa`.
 */
class m170720_173034_add_file_column_to_npa_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('npa', 'file', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('npa', 'file');
    }
}
