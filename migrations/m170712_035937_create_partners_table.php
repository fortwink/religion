<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners`.
 */
class m170712_035937_create_partners_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partners');
    }
}
