<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `image`.
 */
class m170710_053019_add_sort_column_to_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('image', 'sort', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('image', 'sort');
    }
}
