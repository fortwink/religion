<?php

use yii\db\Migration;

/**
 * Handles adding file to table `npa`.
 */
class m170711_154028_add_file_column_to_npa_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('npa', 'file', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('npa', 'file');
    }
}
