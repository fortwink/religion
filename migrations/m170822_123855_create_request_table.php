<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request`.
 */
class m170822_123855_create_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'map' => $this->string(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('request');
    }
}
