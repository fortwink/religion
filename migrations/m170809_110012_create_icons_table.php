<?php

use yii\db\Migration;

/**
 * Handles the creation of table `icons`.
 */
class m170809_110012_create_icons_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('icons', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('icons');
    }
}
