<?php

use yii\db\Migration;

/**
 * Handles adding category_id to table `npa`.
 */
class m170720_171351_add_category_id_column_to_npa_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('npa', 'category_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('npa', 'category_id');
    }
}
