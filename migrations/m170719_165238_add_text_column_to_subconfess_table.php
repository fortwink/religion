<?php

use yii\db\Migration;

/**
 * Handles adding text to table `subconfess`.
 */
class m170719_165238_add_text_column_to_subconfess_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('subconfess', 'short_text', $this->string());
        $this->addColumn('subconfess', 'full_text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('subconfess', 'short_text');
        $this->dropColumn('subconfess', 'full_text');
    }
}
