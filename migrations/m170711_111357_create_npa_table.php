<?php

use yii\db\Migration;

/**
 * Handles the creation of table `npa`.
 */
class m170711_111357_create_npa_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('npa', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('npa');
    }
}
