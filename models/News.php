<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $created
 * @property int $updated
 * @property string $preview_text
 * @property string $detail_text
 * @property int $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    const TYPE_NEW = 1;
    const TYPE_VIDEO = 2;

    public $picture;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => time(),
            ],
            'image' => [
                //'class' => 'rico\yii2images\behaviors\ImageBehave',
                'class' => 'app\behaviors\ImageBehave',
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','title_kz'], 'required'],
            [['created', 'updated','type','active'], 'integer'],
            [['detail_text','detail_text_kz'], 'string'],
            [['title','preview_text','preview_text_kz', 'date'], 'string', 'max' => 255],
        ];
    }

    public function getDateformat(){
        return implode('.',array_reverse(explode('.',$this->date)));
    }


    public function getNext() {
        $next = $this->find()->where(['>', 'id', $this->id])->one();
        return $next;
    }

    public function getPrev() {
        $prev = $this->find()->where(['<', 'id', $this->id])->orderBy('id desc')->one();
        return $prev;
    }

    public function getName(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'title_'.Lang::getCurrent()->url;
        else {
            $property = 'title';
        }
        return $this->{$property};
    }
    public function getDetail(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'detail_text_'.Lang::getCurrent()->url;
        else {
            $property = 'detail_text';
        }
        return $this->{$property};
    }
    public function getPreview(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'preview_text_'.Lang::getCurrent()->url;
        else {
            $property = 'preview_text';
        }
        return $this->{$property};
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'preview_text' => 'Preview Text',
            'detail_text' => 'Detail Text',
            'date' => 'Date',
        ];
    }
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $file = UploadedFile::getInstance($this, 'picture');
        if (!empty($file) && !$file->hasError) {
            $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
            $file->saveAs($filename);
            $this->removeImages();
            $this->attachImage($filename);
        }
    }
}
