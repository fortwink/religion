<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 10/07/17
 * Time: 11:35
 */

namespace app\models;


class Image extends \rico\yii2images\models\Image
{

    public function rules()
    {
        return
            array_merge(
                [['sort'],'integer'],parent::rules()
            );
    }
}