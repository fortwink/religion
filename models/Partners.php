<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;


/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $name
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\ImageBehave',
            ],
        ];
    }
    public $image;
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['image'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $files = UploadedFile::getInstances($this,'image');
        foreach ($files as $file) {
            if (!empty($file) && !$file->hasError) {
                $this->removeImages();
                $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($filename);
                $this->attachImage($filename);
            }
        }
    }
}
