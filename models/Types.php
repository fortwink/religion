<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "types".
 *
 * @property int $id
 * @property string $name
 */
class Types extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active'], 'integer'],
            [['name','name_kz'], 'string', 'max' => 255],
        ];
    }

    public function getTname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'name_'.Lang::getCurrent()->url;
        else {
            $property = 'name';
        }
        return $this->{$property};
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
