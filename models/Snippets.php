<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "snippets".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $text_kz
 */
class Snippets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'snippets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_kz'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }


    public function getTtext(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'text_'.Lang::getCurrent()->url;
        else {
            $property = 'text';
        }
        return $this->{$property};
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'text_kz' => 'Text Kz',
        ];
    }
}
