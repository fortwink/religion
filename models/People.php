<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "people".
 *
 * @property int $id
 * @property string $name
 * @property int $bdate
 * @property string $gr
 * @property string $register
 * @property int $confess_id
 * @property string $contact_phone
 * @property int $object_id
 * @property string $register_dates
 */
class People extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\ImageBehave',
            ],
        ];
    }
    public $image;

    /**
     * @inheritdoc
     */


    public function getConfession(){
        return $this->hasOne(Confession::className(),['id'=>'confess_id']);
    }

    public function rules()
    {
        return [
            [['name', 'bdate', 'gr', 'confess_id'], 'required'],
            [['subconfess_id','confess_id', 'object_id'], 'integer'],
            [['bdate','name', 'gr', 'register', 'contact_phone', 'register_dates','exp','description','position'], 'string'],
            [['hidden_info'], 'string'],
            [['image'], 'file', 'extensions' => 'jpg, gif, png']
           // [[],'file','allow'=>'']
        ];
    }

    public function getObject() {
        return $this->hasOne(Objects::className(),['id'=>'object_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'position' => 'Должность',
            'bdate' => 'Дата рождения',
            'gr' => 'Гражданство',
            'register' => 'Место, адрес рождения, национальность(скрытое поле)',
            'confess_id' => 'Конфессия',
            'description' => 'Образование, специальность, курсы повышения квалификации',
            'contact_phone' => 'Контакты',
            'object_id' => 'Привязка к объекту(необязательное)',
            'exp' => 'Стаж работы',
        ];
    }
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $files = UploadedFile::getInstances($this,'image');
        foreach ($files as $file) {
            if (!empty($file) && !$file->hasError) {
                $this->removeImages();
                $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($filename);
                $this->attachImage($filename);
            }
        }
    }

}
