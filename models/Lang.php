<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Lang extends Model {


    public static $langs = [
        'ru'=>[
            'ru',
            'ru-RU',
            'Русский'
        ],
        'kz'=>[
            'kz',
            'kz-KZ',
            'Kazakh'
        ],

    ];




    public static function getCurLang($lang=null) {

        $ru = new \stdClass();
        $ru->url = 'ru';
        $ru->local = 'ru-RU';
        $ru->name = 'Русский';

        $kz = new \stdClass();
        $kz->url = 'kz';
        $kz->local = 'kz_KZ';
        $kz->name = 'Kazakh';

        $langs = [
            'ru'=>$ru,
            'kz'=>$kz,
        ];




        if (!empty($lang) && !empty($langs[$lang]))
            return $langs[$lang];
        else
            return $langs['ru'];
    }
    //Переменная, для хранения текущего объекта языка
    static $current = null;

//Получение текущего объекта языка
    static function getCurrent()
    {

        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

//Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null)
    {

        $language = self::getCurLang($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->local;
    }

//Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return Lang::$langs['ru'];
    }

//Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = Lang::getCurLang($url);
            if ( $language === null ) {
                return null;
            }else{
                return $language;
            }
        }
    }

}