<?php

namespace app\models;

use app\models\search\People;
use Yii;
use yii\web\UploadedFile;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property int $confess_id
 * @property string $address
 * @property string $contacts
 * @property int $region_id
 * @property string $description
 * @property string $preview_text
 * @property string $hidden_text
 * @property string $missioners
 * @property string $map
 * @property int $type_id
 * @property int $style_id
 * @property int $icon_id
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'objects';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\ImageBehave',
            ],
           /* [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => time(),
            ],*/
           /* 'manyToMany' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'tags_ids' => 'tags',
                    'lists_ids' => 'lists',
                    'types_ids' => 'types',
                ],
            ],*/
           /* 'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'isDeleted' => true
                ],
                'replaceRegularDelete' => true // mutate native `delete()` method
            ],*/
        ];
    }

    public $preview_pic;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name','name_kz', 'short_name_kz'], 'required'],
            [['confess_id', 'subconfess_id', 'region_id', 'type_id', 'style_id', 'icon_id', 'status'], 'integer'],
            [['description_kz', 'preview_text_kz','description', 'preview_text', 'hidden_text', 'hidden_text_kz', 'missioners'], 'string'],
            [['name', 'short_name', 'address', 'contacts', 'map'], 'string', 'max' => 255],
        ];
    }

    public function getIcon(){
        return $this->hasOne(Icons::className(),['id'=>'icon_id']);
    }

    public function getTname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'name_'.Lang::getCurrent()->url;
        else {
            $property = 'name';
        }
        return $this->{$property};
    }  public function getThidden_text(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'hidden_text_'.Lang::getCurrent()->url;
        else {
            $property = 'hidden_text';
        }
        return $this->{$property};
    }

    public function getTshortname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'short_name_'.Lang::getCurrent()->url;
        else {
            $property = 'short_name';
        }
        return $this->{$property};
    }
    public function getDesc(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'description_'.Lang::getCurrent()->url;
        else {
            $property = 'description';
        }
        return $this->{$property};
    }
    public function getPreview(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'preview_text_'.Lang::getCurrent()->url;
        else {
            $property = 'preview_text';
        }
        return $this->{$property};
    }



    public function getPeople () {
        return $this->hasMany(People::className(),['object_id'=>'id']);
    }
    public function getConfession (){
        return $this->hasOne(Confession::className(),['id'=>'confess_id']);
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Полное наименование',
            'short_name' => 'Краткое наименование',
            'confess_id' => 'Конфессия',
            'address' => 'Адрес',
            'contacts' => 'Контактные данные',
            'region_id' => 'Район',
            'description' => 'Подробное описание',
            'description_kz' => 'Подробное описание KZ',
            'preview_text' => 'Краткое описание',
            'preview_text_kz' => 'Краткое описание KZ',
            'hidden_text' => 'Скрытое описание',
            'hidden_text_kz' => 'Скрытое описание KZ',
            'missioners' => 'Миссионеры',
            'map' => 'Координаты на карте',
            'type_id' => 'Тип объекта',
            'status' => 'Скрытый объект',
            'style_id' => 'Стиль объекта',
            'icon_id' => 'Иконка объекта(опционально)',
        ];
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $files = UploadedFile::getInstances($this,'preview_pic');
        foreach ($files as $file) {
            if (!empty($file) && !$file->hasError) {
                $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($filename);
                $this->attachImage($filename);
            }
        }
    }
}
