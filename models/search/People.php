<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\People as PeopleModel;

/**
 * People represents the model behind the search form of `app\models\People`.
 */
class People extends PeopleModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bdate', 'confess_id', 'object_id'], 'integer'],
            [['name', 'gr', 'register', 'contact_phone', 'register_dates'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PeopleModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bdate' => $this->bdate,
            'confess_id' => $this->confess_id,
            'object_id' => $this->object_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'gr', $this->gr])
            ->andFilterWhere(['like', 'register', $this->register])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'register_dates', $this->register_dates]);

        return $dataProvider;
    }
}
