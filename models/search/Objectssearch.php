<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Objects;

/**
 * Objectssearch represents the model behind the search form of `app\models\Objects`.
 */
class Objectssearch extends Objects
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'confess_id', 'region_id', 'type_id', 'style_id', 'icon_id','status'], 'integer'],
            [['name', 'short_name', 'address', 'contacts', 'description', 'preview_text', 'hidden_text', 'missioners', 'map'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Objects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'confess_id' => $this->confess_id,
            'region_id' => $this->region_id,
            'type_id' => $this->type_id,
            'style_id' => $this->style_id,
            'icon_id' => $this->icon_id,
            'status'=>$this->status
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'preview_text', $this->preview_text])
            ->andFilterWhere(['like', 'hidden_text', $this->hidden_text])
            ->andFilterWhere(['like', 'missioners', $this->missioners])
            ->andFilterWhere(['like', 'map', $this->map]);

        return $dataProvider;
    }
}
