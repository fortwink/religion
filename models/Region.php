<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "region".
 *
 * @property int $id
 * @property string $name
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name','name_kz'], 'string', 'max' => 255],
        ];
    }

    public function getTname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'name_'.Lang::getCurrent()->url;
        else {
            $property = 'name';
        }
        return $this->{$property};
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
