<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "confession".
 *
 * @property int $id
 * @property string $name
 */
class Confession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\ImageBehave',
            ],
        ];
    }
    public $image;

    public static function tableName()
    {
        return 'confession';
    }


    public function getPeople(){
        return $this->hasMany(People::className(),['confess_id'=>'id']);
    }

    public function getPeople6(){
        return $this->hasMany(People::className(),['confess_id'=>'id'])->limit(6);
    }

    public function getObjects(){
        return $this->hasMany(Objects::className(),['confess_id'=>'id'])->where(['status'=>0]);
    }

    public function getObjects4(){
        return $this->hasMany(Objects::className(),['confess_id'=>'id'])->where(['status'=>0])->limit(4);
    }

    public function getTname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'name_'.Lang::getCurrent()->url;
        else {
            $property = 'name';
        }
        return $this->{$property};
    }

    public function getShort(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'short_text_'.Lang::getCurrent()->url;
        else {
            $property = 'short_text';
        }
        return $this->{$property};
    }
    public function getFull(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'full_text_'.Lang::getCurrent()->url;
        else {
            $property = 'full_text';
        }
        return $this->{$property};
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','name_kz'], 'required'],
            [['name','short_text','short_text_kz'], 'string', 'max' => 255],
            [['full_text','full_text_kz'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $files = UploadedFile::getInstances($this,'image');
        foreach ($files as $file) {
            if (!empty($file) && !$file->hasError) {
                $this->removeImages();
                $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($filename);
                $this->attachImage($filename);
            }
        }
    }

}
