<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "subconfess".
 *
 * @property int $id
 * @property string $name
 * @property int $protest
 */
class Subconfess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subconfess';
    }


    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'app\behaviors\ImageBehave',
            ],
        ];
    }
    public $image;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['protest'], 'integer'],
            [['name','short_text','name_kz','short_text_kz'], 'string', 'max' => 255],
            [['full_text','full_text_kz'], 'string'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'protest' => 'Протестанты',
        ];
    }
    public function getTname(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'name_'.Lang::getCurrent()->url;
        else {
            $property = 'name';
        }
        return $this->{$property};
    }

    public function getShort(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'short_text_'.Lang::getCurrent()->url;
        else {
            $property = 'short_text';
        }
        return $this->{$property};
    }
    public function getFull(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'full_text_'.Lang::getCurrent()->url;
        else {
            $property = 'full_text';
        }
        return $this->{$property};
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        $files = UploadedFile::getInstances($this,'image');
        foreach ($files as $file) {
            if (!empty($file) && !$file->hasError) {
                $this->removeImages();
                $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($filename);
                $this->attachImage($filename);
            }
        }
    }


}
