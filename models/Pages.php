<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $title
 * @property string $title_kz
 * @property string $text
 * @property string $text_kz
 * @property string $sef
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_kz','text_hidden','text_hidden_kz'], 'string'],
            [['title', 'title_kz', 'sef'], 'string', 'max' => 255],
        ];
    }

    public function getTtitle(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'title_'.Lang::getCurrent()->url;
        else {
            $property = 'title';
        }
        return $this->{$property};
    }
    public function getTtext(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'text_'.Lang::getCurrent()->url;
        else {
            $property = 'text';
        }
        return $this->{$property};
    }
    public function getTtext_hidden(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'text_hidden_'.Lang::getCurrent()->url;
        else {
            $property = 'text_hidden';
        }
        return $this->{$property};
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_kz' => 'Title Kz',
            'text' => 'Text',
            'text_kz' => 'Text Kz',
            'sef' => 'Sef',
        ];
    }
}
