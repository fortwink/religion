<?php

namespace app\models;

use Yii;

use yii\web\UploadedFile;
/**
 * This is the model class for table "icons".
 *
 * @property int $id
 * @property string $name
 */
class Icons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'icons';
    }

    public function behaviors()
    {
        return [

            'image' => [
                //'class' => 'rico\yii2images\behaviors\ImageBehave',
                'class' => 'app\behaviors\ImageBehave',
            ]

        ];
    }
    public $picture;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $file = UploadedFile::getInstance($this, 'picture');
        if (!empty($file) && !$file->hasError) {
            $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
            $file->saveAs($filename);
            $this->removeImages();
            $this->attachImage($filename);
        }
    }
}
