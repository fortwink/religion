<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * ContactForm is the model behind the contact form.
 */
class RequestForm extends \yii\db\ActiveRecord
{
   /* public $name;
    public $email;
    public $subject;
    public $phone;
    public $body;
    public $map;*/
    public $verifyCode;


    public static function tableName()
    {
        return 'request';
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name','email', 'body','phone'], 'required'],
            [['map','file'],'string'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
             /* \Yii::$app->mailer->compose('store/letter', [
                'id' => $id,
                'model'=>$model,
                'user'=>User::findOne($uid),
                'price'=>$model->price_offline,
                'uid'=>$uid
            ])

                ->setFrom(['web.sendmailsite@gmail.com' => 'Новый заказ'])
                ->setTo(['iq.macross@gmail.com','tatyana.troshina@carlsberg.kz','Viktor.Miroshnichenko@carlsberg.kz'])
                //->setTo([])
                ->setSubject('Новый заказ с сайта Salesteam.kz')
                ->send();*/

            //$this->save(false);

            Yii::$app->mailer->compose()
                ->setTo($email)
                //-setReplyTo()
                ->setFrom(['web.sendmailsite@gmail.com' => 'Заявка с сайта'])
                ->setSubject('Новая заявка с сайта')
                ->setHtmlBody(
                    'ФИО: '.$this->name.'<br>'.
                    'E-mail: '.$this->email.'<br>'.
                    'Телефон: '.$this->phone.'<br>'.
                    'Сообщение: '.$this->body.'<br>'.
                    'Координаты: '.$this->map.'<br>'.
                    'File: '.'<a href="'.$this->file.'">'.$this->file.'</a>'
                )
                ->send();
            return true;
        }
        return false;
    }


    public function beforeSave($insert)
    {
        $file = UploadedFile::getInstance($this,'file');

        if (!empty($file) && !$file->hasError) {
            $filename = 'uploads/request/' . $file->baseName.'-'.microtime(true). '.' . $file->extension;
            $file->saveAs($filename);
            $this->file = $filename;
        }

        return parent::beforeSave($insert);
    }

}
