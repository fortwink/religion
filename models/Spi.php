<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "spi".
 *
 * @property int $id
 * @property string $title
 * @property string $title_kz
 * @property string $text
 * @property string $text_kz
 */
class Spi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'spi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'text_kz'], 'string'],
            [['title', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    public function getTtitle(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'title_'.Lang::getCurrent()->url;
        else {
            $property = 'title';
        }
        return $this->{$property};
    }
    public function getTtext(){
        if (Lang::getCurrent()->url != 'ru')
            $property = 'text_'.Lang::getCurrent()->url;
        else {
            $property = 'text';
        }
        return $this->{$property};
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_kz' => 'Title Kz',
            'text' => 'Text',
            'text_kz' => 'Text Kz',
        ];
    }
}
