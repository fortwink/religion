<?php

namespace app\controllers;
use yii\web\NotFoundHttpException;
use app\models\Npa;
use yii\data\Pagination;

class NpaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Npa::find();//->where(['active' => 1])->orderBy('date DESC');
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize'=>12
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'model' => $models,
            'pages' => $pages,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view',compact('model'));
    }

    protected function findModel($id)
    {
        if (($model = Npa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
