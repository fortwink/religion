<?php

namespace app\controllers;
use app\models\News;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class NewsController extends \yii\web\Controller
{
    public function actionIndex($filter=null)
    {

        $query = News::find()->where(['active' => 1])->orderBy('date DESC');
        if ($filter)
            $query->andWhere(['type'=>$filter]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize'=>6
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        //echo '<pre style="display: none;">'.print_r(count($models),1).'</pre>';

        return $this->render('index', [
            'model' => $models,
            'pages' => $pages,
            'filter'=>$filter
        ]);

        //return $this->render('index');

    }

    public function actionView($id=null)
    {

        $model = $this->findModel($id);

        return $this->render('view',compact('model'));
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
