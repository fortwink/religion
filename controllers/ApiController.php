<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 21.06.2017
 * Time: 14:38
 */

namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use app\models\Objects;

class ApiController extends ActiveController
{
    public $modelClass = 'app\models\Objects';

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            //'view' => ['GET', 'HEAD'],
            //'create' => ['POST'],
           // 'update' => ['PUT', 'PATCH'],
            //'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {

        //echo "<pre>".print_r(parent::actions(),1)."</pre>";die;

        $actions = parent::actions();
        unset($actions['index']);
        //return $actions;

        return array_merge([
            'filter' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'prepareDataProvider' => function($action) {
                    //var_dump($action); die;
                    $modelClass = $action->modelClass;
                    if (isset($_GET['list_id'])){

                    }
                    $fil_str = [];
                    $fil_arr = [];
                    foreach ($_GET as $key=>$val){
                        $fil_str[] = $key.'=:'.$key;
                        $fil_arr[':'.$key] = $val;
                    }
                    return new ActiveDataProvider([
                        'query' => $modelClass::find()->where(implode(' AND ',$fil_str), $fil_arr),
                    ]);
                },
                // ...
            ],
        ],$actions);
    }

   /* public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }*/

    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Objects::find()->with('confession'),
            'pagination' => false,
        ]);
        return $activeData;
    }


    /* public function actionIndex($filter = null)
     {
         $modelClass = $this->modelClass;

         $filter =  json_decode($filter,true);

         if ($filter){
             return new ActiveDataProvider([
                 'query' => $modelClass::find()->andWhere($filter),
             ]);
         } else {
             return new ActiveDataProvider([
                 'query' => $modelClass::find(),
             ]);
         }
     }*/
}