<?php

namespace app\controllers;

use app\models\Confession;
use app\models\Objects;
use app\models\People;
use app\models\search\Subconfess;
use app\models\Types;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ConfessionController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($id=null,$conf=null)
    {
        if ($conf) {
            $model = \app\models\Subconfess::findOne($conf);
            $people6 = People::find()->where(['subconfess_id'=>$conf])->limit(6)->all();
            $objects4 = Objects::find()->where(['subconfess_id'=>$conf,'status'=>0])->limit(4)->all();
        } else {
            $model = $this->findModel($id);
            $people6 = $model->people6;
            $objects4 = $model->objects4;
        }
       // echo '<pre>'.print_r($objects4,1).'</pre>'; die;
        return $this->render('view',compact('model','people6','objects4','id','conf'));
    }

    public function actionObjectview($id=null){
        $model = $this->findObject($id);
        return $this->render('objectview',compact('model'));

    }
    public function actionObjects($confess=null,$conf=null,$type=null,$region=null,$q=null){
        /*$model = $this->findModel($confess);

        return $this->render('objects',compact('model'));*/



        $arFilter = [];
        if (\Yii::$app->user->isGuest)
            $arFilter['status'] = 0;

        $confm = null;


        if ($confess) {
            $arFilter['confess_id'] = $confess;
            $confm = Confession::findOne($confess);

        }
        if ($conf) {
            $arFilter['subconfess_id'] = $conf;
        }
        if (\Yii::$app->user->isGuest)
        {
            $aprooved = ArrayHelper::getColumn(Types::find()->where(['active'=>1])->all(),'id');
        }
        else {
            $aprooved = ArrayHelper::getColumn(Types::find()->all(),'id');
        }
        if ($type && in_array($type,$aprooved)) {
            $arFilter['type_id'] = $type;
        } else {
                $arFilter['type_id'] = $aprooved;
        }
        if ($region) {
            $arFilter['region_id'] = $region;
        }


        $query = Objects::find()->where($arFilter)->orderBy('id DESC');

        if ($q) {
            $query->andWhere(
                'name'           .' like "%'.$q.'%" OR '.
                'name_kz'        .' like "%'.$q.'%" OR '.
                'address'        .' like "%'.$q.'%" OR '.
                'contacts'       .' like "%'.$q.'%" OR '.
                'description_kz' .' like "%'.$q.'%" OR '.
                'preview_text_kz'.' like "%'.$q.'%" OR '.
                'description'    .' like "%'.$q.'%" OR '.
                'preview_text'   .' like "%'.$q.'%" '

               );
            //$arFilter['subconfess_id'] = $conf;
        }




       // echo '<pre>'.print_r($query->all(),1).'</pre>';die;

        $countQuery = clone $query;
        $countQuery1 = clone $query;
        $modelraw = $countQuery1->all();

        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize'=>16
        ]);
        $model = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('objects', compact('model','pages','confess','conf','region','type','modelraw','confm'));
    }

    public function actionMissionerview($id=null){
        $model = $this->findPeople($id);
        return $this->render('missioner_view',compact('model'));

    }


    public function actionMissioners($confid=null,$conf=null){
        /*$model = $this->findModel($confess);

        return $this->render('objects',compact('model'));*/
        $filter = [];
        if ($conf){
            $filter['subconfess_id']= $conf;
        }

        if ($confid){
            $filter['confess_id'] = $confid;
        }
        $query = People::find()->where($filter)->orderBy('id DESC');
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize'=>15
        ]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('missioners', [
            'model' => $models,
            'pages' => $pages,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Confession::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findObject($id)
    {
        $user_auth = [];
        if (\Yii::$app->user->isGuest)
            $user_auth = ['status'=>0];
        if (($model = Objects::find()->where(array_merge(['id'=>$id],$user_auth))->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findPeople($id)
    {
        if (($model = People::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
