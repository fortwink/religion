<?php

namespace app\controllers;

use app\models\Confession;
use app\models\Objects;
use app\models\Partners;
use app\models\Region;
use app\models\RequestForm;
use app\models\Snippets;
use app\models\Spi;
use app\models\Types;
use app\models\Lang;
use Yii;
use app\models\Npa;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionGetfilters () {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $filters = [];

        $region = Region::find()->all();
        $confess = Confession::find()->all();

        if (Yii::$app->user->isGuest)
        {
            $type = Types::find()->where(['active'=>1])->all();
        }
        else {
            $type = Types::find()->all();
        }
        //$type = Types::find()->where(['active'=>1])->all();

        $filters['confess'] = $confess;
        $filters['type'] = $type;
        $filters['region'] = $region;

        return $filters;
    }

    public function actionGetloc ($confess_id=null,$region_id=null,$type_id=null,$q=null) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $filter = [];
        if (Yii::$app->user->isGuest)
            $filter['status']=0;

        if ($confess_id){
            $filter['confess_id']=$confess_id;
        }
        if ($region_id){
            $filter['region_id']=$region_id;
        }
        if (Yii::$app->user->isGuest)
        {
            $aprooved = ArrayHelper::getColumn(Types::find()->where(['active'=>1])->all(),'id');
        }
        else {
            $aprooved = ArrayHelper::getColumn(Types::find()->all(),'id');
        }
        if ($type_id && in_array($type_id,$aprooved)) {
            $filter['type_id'] = $type_id;
        } else {
                $filter['type_id'] = $aprooved;
        }
        /*if ($type_id){
            $filter['type_id']=$type_id;
        }
        /*if ($q){
            $filter['type_id']=$type_id;
        }*/



        $model = Objects::find()->where($filter)->with('confession');

        if ($q){

            $model->andWhere('
                name like :q OR
                name_kz like :q OR
                address like :q OR
                contacts like :q OR
                description_kz like :q OR
                description like :q OR
                preview_text_kz like :q OR
                preview_text like :q
            ',[':q'=>'%'.$q.'%']);
        }

        $model = $model->all();
        $output = [];
        foreach ($model as $item) {
            $hasicon = $item->icon;
            $tmp = $item->toArray();
            $tmp['image'] = '/'.$item->getImage()->getPath('100x100');
            $tmp['iconpic']='';
            switch ($item['confess_id']) {
                case 1: $tmp['iconpic'] = '/images/icon_islam.png';break;
                case 2: $tmp['iconpic'] = '/images/icon_hrist.png'; break;
                case 3: $tmp['iconpic'] = '/images/icon_bahai.png';break;
                case 4: $tmp['iconpic'] = '/images/icon_krishna.png';break;
                case 5: $tmp['iconpic'] = '/images/icon_buddizm.png';break;
                case 6: $tmp['iconpic'] = '/images/icon_iud.png';break;
                case 7: $tmp['iconpic'] = '/images/new.png';break;
            }
            if ($item['type_id']==2){
                $tmp['iconpic'] = '/images/stationar_icon.png';
            }
            if ($hasicon){
               $tmp['iconpic'] = '/'.$hasicon->getImage()->getPath('100x100');
            }
            $tmp['confession'] = '';
            $tmp['name'] = $item->tname;
            $tmp['short_name'] = $item->tshortname;
            if ($item->confession){
                $tmp['confession'] = $item->confession->tname;
            }
            $output[] = $tmp;

        }
        return $output;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        

        $partners = Partners::find()->all();
        $npa = Npa::find()->limit(8)->all();
        $video = Snippets::findOne(1);

        return $this->render('index',compact('partners','npa','video'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(['religionmap17@gmail.com','akbota.serik.94@mail.ru', 'info@religionmap.kz'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->redirect('/pages/success');

        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        if (Lang::getCurrent()->url != 'ru')
            return $this->render('about_kz');
        else
            return $this->render('about');
    }

    public function actionRequest()
    {
        $model = new RequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->save() && $model->contact(['religionmap17@gmail.com','akbota.serik.94@mail.ru', 'info@religionmap.kz'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->redirect('/pages/success');
        }
        return $this->render('request', [
            'model' => $model,
        ]);
    }

    public function actionFaq()
    {
        $model = Spi::find()->all();
        return $this->render('faq',compact('model'));
    }
}
