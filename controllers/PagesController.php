<?php

namespace app\controllers;

use app\models\search\Pages;

class PagesController extends \yii\web\Controller
{
    public function actionIndex($id=null)
    {
        $model = Pages::find()->where(['sef'=>$id])->one();

        return $this->render('index', compact('model'));
    }

}
